﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace GetPosition {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        KinectSensor kinect;
        MultiSourceFrameReader multiReader;
        FrameDescription depthFrameDesc;

        //表示用
        WriteableBitmap depthImage;
        ushort[] depthBuffer;
        byte[] depthBitmapBuffer;
        int depthStride;
        Int32Rect depthRect;

        Body[] bodies;
        JointType[] jointType = new[] {JointType.HandRight,
                                       JointType.SpineShoulder,
                                       };
        int jointTypeNumber;
        int a = 0; //骨格追跡後の改行用
        StreamWriter streamWriter;

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            try {
                jointTypeNumber = jointType.Length;
                string filename = @"Data\" + GetMotionCode() + "_" + GetNameCode() + "_" + GetNumberCode() +".txt";
                streamWriter = new StreamWriter(filename, false, Encoding.GetEncoding("Shift_Jis"));

                //kinectと接続                
                kinect = KinectSensor.GetDefault();
                kinect.Open();

                depthFrameDesc = kinect.DepthFrameSource.FrameDescription;

                //bodyを入れる配列を作る
                bodies = new Body[kinect.BodyFrameSource.BodyCount];

                //フレームリーダーを開く(データの読み込み準備)
                multiReader = kinect.OpenMultiSourceFrameReader(
                    //FrameSourceTypes.Depth |
                    FrameSourceTypes.Body);
                multiReader.MultiSourceFrameArrived += multiReader_MultiSourceFrameArrived;

                depthImage = new WriteableBitmap(
                                    depthFrameDesc.Width, depthFrameDesc.Height,
                                    96, 96, PixelFormats.Gray8, null); //外枠
                depthBuffer = new ushort[depthFrameDesc.LengthInPixels];
                depthBitmapBuffer = new byte[depthFrameDesc.LengthInPixels];
                depthRect = new Int32Rect(0, 0, depthFrameDesc.Width, depthFrameDesc.Height);
                depthStride = (int)(depthFrameDesc.Width); //1行の文字数   

                //DepthImage.Source = depthImage;
            } catch(Exception ex) {
                MessageBox.Show(ex.Message);
                Close();
            }
        }

        void multiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            var multiFrame = e.FrameReference.AcquireFrame();
            if(multiFrame == null) {
                return;
            }

            //UpdateDepthFrame(multiFrame);
            UpdateBodyFrame(multiFrame);

            DrawDepthFrame();
        }

        private void UpdateDepthFrame(MultiSourceFrame e) {
            //深度フレームを取得
            using(var depthFrame = e.DepthFrameReference.AcquireFrame()) {
                if(depthFrame == null) {
                    return;
                }

                //depthデータを取得
                depthFrame.CopyFrameDataToArray(depthBuffer);
            }
        }

        private void UpdateBodyFrame(MultiSourceFrame e) {
            using(var bodyFrame = e.BodyFrameReference.AcquireFrame()) {
                if(bodyFrame == null) {
                    return;
                }

                //bodyデータを取得
                bodyFrame.GetAndRefreshBodyData(bodies);
            }
        }

        private void DrawDepthFrame() {
            DrawFrame();

            //for(int i = 0; i < depthBuffer.Length; i++) {
            //    depthBitmapBuffer[i] = (byte)(depthBuffer[i] % 255);
            //}
            //depthImage.WritePixels(depthRect, depthBitmapBuffer, depthStride, 0);
        }

        private void DrawFrame() {
            //追跡しているBodyのみループする
            foreach(var body in bodies.Where(b => b.IsTracked)) {
                foreach(var joint in body.Joints) {
                    for(int i = 0; i < jointType.Length; i++) {
                        if(joint.Value.JointType == jointType[i]) {
                            if(joint.Value.TrackingState == TrackingState.Tracked) {  //手の位置が追跡状態
                                DrawEllipse(joint.Value, 5, Brushes.Blue);
                            } else if(joint.Value.TrackingState == TrackingState.Inferred) {  //手の位置が推測状態
                                DrawEllipse(joint.Value, 5, Brushes.Yellow);
                            }
                        }
                    }
                }
            }
        }

        private void DrawEllipse(Joint joint, int R, Brush brush) {
            var ellipse = new Ellipse() {
                Width = R,
                Height = R,
                Fill = brush,
            };

            //カメラ座標系をColor座標系に変換する
            var point = kinect.CoordinateMapper.MapCameraPointToDepthSpace(joint.Position);
            if(point.X < 0 || point.Y < 0) {
                return;
            }

            //Color座標系で円を配置する
            Canvas.SetLeft(ellipse, point.X - (R / 2));
            Canvas.SetTop(ellipse, point.Y - (R / 2));
            CanvasBody.Children.Add(ellipse);

            streamWriter.Write("{0},{1},{2},",joint.Position.X * 100, joint.Position.Y * 100, joint.Position.Z * 100);
            a++;
            if(joint.JointType == JointType.SpineShoulder) {
                streamWriter.Write(Environment.NewLine);
            }
        }

        private string GetMotionCode() {
            if(Go.IsChecked == true) {
                return "Agree";
            } else if(Come.IsChecked == true) {
                return "River";
            } else if(See.IsChecked == true) {
                return "Sister";
            } else if(Shortgo.IsChecked == true) {
                return "Thank";
            } else if(Loopgo.IsChecked == true) {
                return "Very";
            } else {
                return "";
            }
        }

        private string GetNameCode() {
            if(Iz.IsChecked == true) {
                return "Iz";
            } else if(Su.IsChecked == true) {
                return "Su";
            } else if(Yaj.IsChecked == true) {
                return "Yaj";
            } else {
                return "";
            }
        }

        private string GetNumberCode() {
            if(one.IsChecked == true) {
                return "1";
            } else if(two.IsChecked == true) {
                return "2";
            } else { 
                return "";
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e) {
            streamWriter.Close();
            string filename = @"Data\" + GetMotionCode() + "_" + GetNameCode() + "_" + GetNumberCode() + ".txt";
            streamWriter = new StreamWriter(filename, false, Encoding.GetEncoding("Shift_Jis"));
        }
    }
}
