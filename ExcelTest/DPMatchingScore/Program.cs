﻿using ClosedXML.Excel;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DPMatchingScore {
    class Program {
        static void Main(string[] args) {
            const int DATASIZE = 26;
            string[] Name = new string[] {
                "Hujita", "Izumi", "Oguri",
                "Ohta", "Sugiura", "Suzuki",
                "Underson", "Yajima","Yamada", "Yoshii"
            };
            string[] Word = new string[] {
                "red", "blue", "morning", "hot",
                "thank", "go", "sister", "beautiful",
                "yesterday", "adult", "sad", "Tuesday",
                "river", "Cloudy", "come", "Monday",
                "SL.", "white", "Wednesday", "cool",
                "sunny", "left", "right", "everyone",
                "Thursday"
            };
            string beginerfilename;
            List<DataArray> list2 = new List<DataArray>();
            
            for(int i = 0; i < Name.Count(); i++) {
                string filename = Name[i] + ".xlsx";
                var workbook = new XLWorkbook();
                for(int j = 1; j < DATASIZE; j++) {
                    string sheetname = Word[j - 1];
                    var worksheet = workbook.Worksheets.Add(sheetname);
                    list2.Clear();
                    beginerfilename = @"Data\" + Name[i] + "\\" + j + "_R.txt";
                    FileRead(list2, beginerfilename);
                    for(int m = 0; m < list2.Count; m++) {
                        var cell = worksheet.Cell(m + 1, 1);
                        cell.Value = list2[m].X1;
                        cell = worksheet.Cell(m + 1, 2);
                        cell.Value = list2[m].Y1;
                        cell = worksheet.Cell(m + 1, 3);
                        cell.Value = list2[m].Z1;
                        cell = worksheet.Cell(m + 1, 4);
                        cell.Value = list2[m].X2;
                        cell = worksheet.Cell(m + 1, 5);
                        cell.Value = list2[m].Y2;
                        cell = worksheet.Cell(m + 1, 6);
                        cell.Value = list2[m].Z2;
                        cell = worksheet.Cell(m + 1, 8);
                        cell.Value = list2[m].X1 - list2[m].X2;
                        cell = worksheet.Cell(m + 1, 9);
                        cell.Value = list2[m].Y1 - list2[m].Y2;
                        cell = worksheet.Cell(m + 1, 10);
                        cell.Value = list2[m].Z1 - list2[m].Z2;
                        if(m > 0) {
                            cell = worksheet.Cell(m + 1, 12);
                            cell.Value = (list2[m].X1 - list2[m].X2) - (list2[m - 1].X1 - list2[m - 1].X2);
                            cell = worksheet.Cell(m + 1, 13);
                            cell.Value = (list2[m].Y1 - list2[m].Y2) - (list2[m - 1].Y1 - list2[m - 1].Y2);
                            cell = worksheet.Cell(m + 1, 14);
                            cell.Value = (list2[m].Z1 - list2[m].Z2) - (list2[m - 1].Z1 - list2[m - 1].Z2);
                        }
                    }
                }
                workbook.SaveAs(filename);
            }
        }

        private static bool FileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 6;
            StreamReader sr;
            DataArray tmpData = new DataArray();
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            } catch {
                return false;
            }

            while(sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for(i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    } catch(FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    } catch(OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }

                tmpData.X1 = result[0];
                tmpData.Y1 = result[1];
                tmpData.Z1 = result[2];
                tmpData.X2 = result[3];
                tmpData.Y2 = result[4];
                tmpData.Z2 = result[5];
                list.Add(tmpData);
            }
            sr.Close();
            return true;
        }

        //DataArray型構造体
        struct DataArray {
            public float X1;
            public float Y1;
            public float Z1;
            public float X2;
            public float Y2;
            public float Z2;
        }
    }
}