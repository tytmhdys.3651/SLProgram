﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPMatchingScore {
    class Program {
        static int xFreq, yFreq, zFreq;

        static void Main(string[] args) {
            const int DATASIZE = 26;
            string[] Name = new string[] {
                "Hujita",
                "Oguri",
                "Ohta",
                "Sugiura",
                "Suzuki",
                "Underson",
                "Yajima",
                "Yamada",
                "Yoshii"
            };
            string[] Word = new string[] {
                "red", "blue", "morning", "hot",
                "thank", "go", "sister", "beautiful",
                "yesterday", "adult", "sad", "Tuesday",
                "river", "Cloudy", "come", "Monday",
                "SL.", "white", "Wednesday", "cool",
                "sunny", "left", "right", "everyone",
                "Thursday"
            };
            //List<string> masterfilenameList = new List<string>() {
            //    @"Data\Go_Iz_3.txt",
            //    @"Data\Come_Iz_1.txt",                
            //    @"Data\See_Iz_2.txt",
            //    @"Data\Very_Iz_1.txt",              
            //    @"Data\Agree_Iz_1.txt",                
            //    @"Data\Thank_Iz_1.txt",               
            //    @"Data\River_Iz_1.txt",
            //    @"Data\Sister_Iz_1.txt",               
            //    };
            //List<string> beginerfilenameList = new List<string>() {
            //    @"Data\Go_Su_1.txt",
            //    @"Data\Go_Yaj_1.txt",
            //    @"Data\Come_Su_2.txt",
            //    @"Data\Come_Yaj_2.txt",
            //    @"Data\See_Su_1.txt",
            //    @"Data\See_Yaj_2.txt",
            //    @"Data\Very_Su_1.txt",
            //    @"Data\Very_Yaj_1.txt",
            //    @"Data\Agree_Su_1.txt",
            //    @"Data\Agree_Yaj_1.txt",
            //    @"Data\Thank_Su_1.txt",
            //    @"Data\Thank_Yaj_1.txt",
            //    @"Data\River_Su_1.txt",
            //    @"Data\River_Yaj_1.txt",
            //    @"Data\Sister_Su_1.txt",
            //    @"Data\Sister_Yaj_1.txt",
            //};
            string masterfilename;
            string beginerfilename;
            List<DataArray> list1 = new List<DataArray>();
            List<DataArray> list2 = new List<DataArray>();
            StreamWriter streamWriter = new StreamWriter(@"DPMatchingResult.csv", false, Encoding.Default);

            streamWriter.Write(",");
            for(int i = 0; i < Name.Count() * 25; i++) {
                if(i % 25 == 0) {
                    streamWriter.Write(Name[i / 25] + ",");
                } else {
                    streamWriter.Write(",");
                }
            }
            streamWriter.Write(Environment.NewLine);

            streamWriter.Write(",");
            for(int i = 0; i < Name.Count(); i++) {
                for(int j = 0; j < DATASIZE - 1; j++) {
                    streamWriter.Write(Word[j] + ",");
                }
            }
            streamWriter.Write(Environment.NewLine);

            for(int i = 1; i < DATASIZE; i++) {
                xFreq = 0; yFreq = 0; zFreq = 0;
                list1.Clear();
                list1.Add(ClearDataArray());
                masterfilename = @"Data\DataSet4\Izumi\" + i + "_R.txt";
                streamWriter.Write(Word[i - 1] + ",");
                FileRead(list1, masterfilename);
                MotionCount(list1);
                for(int k = 0; k < Name.Count(); k++) {
                    for(int j = 1; j < DATASIZE; j++) {
                        list2.Clear();
                        list2.Add(ClearDataArray());
                        beginerfilename = @"Data\DataSet4\" + Name[k] + "\\" + j + "_R.txt";
                        FileRead(list2, beginerfilename);
                        double DPscore = DPmatching3(list1, list2);
                        streamWriter.Write(DPscore + ",");
                    }
                }
                streamWriter.Write(Environment.NewLine);
            }
            streamWriter.Close();
        }

        private static void MotionCount(List<DataArray> list1) {
            for(int k = 2; k < list1.Count; k++) {
                if(Math.Abs(list1[k].X - list1[k - 1].X) > 1.0) {
                    xFreq++;
                }
                if(Math.Abs(list1[k].Y - list1[k - 1].Y) > 1.0) {
                    yFreq++;
                }
                if(Math.Abs(list1[k].Z - list1[k - 1].Z) > 1.0) {
                    zFreq++;
                }
            }
        }

        private static bool FileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 6;
            StreamReader sr;
            DataArray tmpData = new DataArray();
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            } catch {
                return false;
            }

            while(sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for(i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    } catch(FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    } catch(OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }

                tmpData.X = result[0] - result[3];
                tmpData.Y = result[1] - result[4];
                tmpData.Z = result[2] - result[5];
                list.Add(tmpData);
            }
            sr.Close();
            return true;
        }

        //DP照合1
        private static float DPmatching(List<DataArray> Master, List<DataArray> Beginer) {
            const int DIAGONAL = 0; //斜め
            const int RIGHTUNDER = 1; //→↓↓
            const int UNDERRIGHT = 2; //↓→→                                      
            int M_FrameNumber = Master.Count; //お手本のフレーム長
            int B_FrameNumber = Beginer.Count; //初心者手話のフレーム長
            int i, j;
            float distance = -1;

            float[,] Cost = new float[M_FrameNumber, B_FrameNumber]; //コスト
            int[,] From = new int[M_FrameNumber, B_FrameNumber]; //どこから来たか
            int[,] c = new int[M_FrameNumber, B_FrameNumber]; //距離

            c[0, 0] = 0;
            Cost[0, 0] = 0;
            From[0, 0] = -1;

            //→方向に初期値設定
            for (i = 1; i < M_FrameNumber; i++) {
                c[i, 0] = 0;
                Cost[i, 0] = float.MaxValue;
                From[i, 0] = -1;
            }
            for (i = 1; i < M_FrameNumber; i++) {
                c[i, 1] = 2;
                Cost[i, 1] = Cost[i - 1, 0] + Calc_Penalty(Master[i], Beginer[1]) * 2;
                From[i, 1] = 0;
            }

            //↓方向に初期値設定
            for (j = 1; j < B_FrameNumber; j++) {
                c[0, j] = 0;
                Cost[0, j] = float.MaxValue;
                From[0, j] = -1;
            }
            for (j = 2; j < B_FrameNumber; j++) {
                c[1, j] = 2;
                Cost[1, j] = Cost[0, j - 1] + Calc_Penalty(Master[1], Beginer[j]) * 2;
                From[1, j] = 0;
            }

            //コスト計算
            for (i = 2; i < M_FrameNumber; i++) {
                for (j = 2; j < B_FrameNumber; j++) {
                    float dtemp1 = Cost[i - 1, j - 1] + Calc_Penalty(Master[i], Beginer[j]) * 2;
                    float dtemp2 = Cost[i - 2, j - 1] + Calc_Penalty(Master[i - 1], Beginer[j]) * 2 + Calc_Penalty(Master[i], Beginer[j]);
                    float dtemp3 = Cost[i - 1, j - 2] + Calc_Penalty(Master[i], Beginer[j - 1]) * 2 + Calc_Penalty(Master[i], Beginer[j]);

                    int min = Min(dtemp1, dtemp2, dtemp3);
                    if (min == DIAGONAL) {
                        c[i, j] = c[i - 1, j - 1] + 2;
                        Cost[i, j] = dtemp1;
                        From[i, j] = DIAGONAL;
                    } else if (min == RIGHTUNDER) {
                        c[i, j] = c[i - 2, j - 1] + 3;
                        Cost[i, j] = dtemp2;
                        From[i, j] = RIGHTUNDER;
                    } else {
                        c[i, j] = c[i - 1, j - 2] + 3;
                        Cost[i, j] = dtemp3;
                        From[i, j] = UNDERRIGHT;
                    }
                }
            }
            distance = Cost[M_FrameNumber - 1, B_FrameNumber - 1] / c[M_FrameNumber - 1, B_FrameNumber - 1];

            i = M_FrameNumber - 1;
            j = B_FrameNumber - 1;
            //while(i > 0 || j > 0) {
            //    Console.Write(i + " " + j + " " + From[i, j] + " " + Cost[i, j] + "\n");
            //    if(From[i, j] == DIAGONAL) {
            //        i = i - 1; j = j - 1;
            //    } else if(From[i, j] == RIGHTUNDER) {
            //        i = i - 2; j = j - 1;
            //    } else if(From[i, j] == UNDERRIGHT) {
            //        j = j - 2; i = i - 1;
            //    }
            //}

            return distance;
        }

        //DP照合2
        private static float DPmatching2(List<DataArray> Master, List<DataArray> Beginer) {
            const int DIAGONAL = 0; //斜め
            const int RIGHT = 1; //→
            const int UNDER = 2; //↓                                     
            int M_FrameNumber = Master.Count; //お手本のフレーム長
            int B_FrameNumber = Beginer.Count; //初心者手話のフレーム長
            int i, j;
            float distance = -1;

            float[,] Cost = new float[M_FrameNumber, B_FrameNumber]; //コスト
            int[,] From = new int[M_FrameNumber, B_FrameNumber]; //どこから来たか
            int[,] c = new int[M_FrameNumber, B_FrameNumber]; //距離

            c[0, 0] = 0;
            Cost[0, 0] = 0;
            From[0, 0] = -1;

            //→方向に初期値設定
            for(i = 1; i < M_FrameNumber; i++) {
                c[i, 0] = 0;
                Cost[i, 0] = float.MaxValue;
                From[i, 0] = -1;
            }
            for(i = 1; i < M_FrameNumber; i++) {
                c[i, 1] = 2;
                Cost[i, 1] = Cost[i - 1, 0] + Calc_Penalty(Master[i], Beginer[1]) * 2;
                From[i, 1] = 0;
            }

            //↓方向に初期値設定
            for(j = 1; j < B_FrameNumber; j++) {
                c[0, j] = 0;
                Cost[0, j] = float.MaxValue;
                From[0, j] = -1;
            }
            for(j = 2; j < B_FrameNumber; j++) {
                c[1, j] = 2;
                Cost[1, j] = Cost[0, j - 1] + Calc_Penalty(Master[1], Beginer[j]) * 2;
                From[1, j] = 0;
            }

            //コスト計算
            for(i = 2; i < M_FrameNumber; i++) {
                for(j = 2; j < B_FrameNumber; j++) {
                    float dtemp1 = Cost[i - 1, j - 1] + Calc_Penalty(Master[i], Beginer[j]) * 2;
                    float dtemp2 = Cost[i - 1, j] + Calc_Penalty(Master[i], Beginer[j]);
                    float dtemp3 = Cost[i, j - 1] + Calc_Penalty(Master[i], Beginer[j]);

                    int min = Min(dtemp1, dtemp2, dtemp3);
                    if(min == DIAGONAL) {
                        c[i, j] = c[i - 1, j - 1] + 2;
                        Cost[i, j] = dtemp1;
                        From[i, j] = DIAGONAL;
                    } else if(min == RIGHT) {
                        c[i, j] = c[i - 1, j] + 1;
                        Cost[i, j] = dtemp2;
                        From[i, j] = RIGHT;
                    } else {
                        c[i, j] = c[i, j - 1] + 1;
                        Cost[i, j] = dtemp3;
                        From[i, j] = UNDER;
                    }
                }
            }
            distance = Cost[M_FrameNumber - 1, B_FrameNumber - 1] / c[M_FrameNumber - 1, B_FrameNumber - 1];
            i = M_FrameNumber - 1;
            j = B_FrameNumber - 1;
            while(i > 0 || j > 0) {
                //Console.Write(From[i, j] + " " + Cost[i,j] + "\n");
                if(From[i,j] == DIAGONAL) {
                    i = i - 1; j = j - 1;
                } else if(From[i,j] == RIGHT) {
                    i = i - 1;
                } else if (From[i,j] == UNDER) {
                    j = j - 1;
                }
            }
            return distance;
        }

        //DP照合3
        private static float DPmatching3(List<DataArray> Master, List<DataArray> Beginer) {                                 
            int M_FrameNumber = Master.Count; //お手本のフレーム長
            int B_FrameNumber = Beginer.Count; //初心者手話のフレーム長
            int i, j;
            float distance = -1;

            DataArray[,] Cost = new DataArray[M_FrameNumber, B_FrameNumber]; //コスト

            Cost[0, 0] = ClearDataArray();

            //→方向に初期値設定
            for(i = 1; i < M_FrameNumber; i++) {
                Cost[i, 0] = MaxValueDataArray();
            }
            for(i = 1; i < M_FrameNumber; i++) {
                Cost[i, 1] = Calc_Add(Cost[i - 1, 0], Calc_Scalor(Calc_Penalty3(Master[i], Beginer[1]), 2), ClearDataArray());
            }

            //↓方向に初期値設定
            for(j = 1; j < B_FrameNumber; j++) {
                Cost[0, j] = MaxValueDataArray();
            }
            for(j = 2; j < B_FrameNumber; j++) {
                Cost[1, j] = Calc_Add(Cost[0, j - 1], Calc_Scalor(Calc_Penalty3(Master[1], Beginer[j]), 2), ClearDataArray());
            }

            //コスト計算
            for(i = 2; i < M_FrameNumber; i++) {
                for(j = 2; j < B_FrameNumber; j++) {
                    //DataArray dtemp1 = Calc_Add(Cost[i - 1, j - 1], Calc_Scalor(Calc_Penalty3(Master[i], Beginer[j]), 2), ClearDataArray());
                    //DataArray dtemp2 = Calc_Add(Cost[i - 2, j - 1], Calc_Scalor(Calc_Penalty3(Master[i - 1], Beginer[j]), 2), Calc_Penalty3(Master[i], Beginer[j]));
                    //DataArray dtemp3 = Calc_Add(Cost[i - 1, j - 2], Calc_Scalor(Calc_Penalty3(Master[i], Beginer[j - 1]), 2), Calc_Penalty3(Master[i], Beginer[j]));

                    DataArray dtemp1 = Calc_Add(Cost[i - 1, j - 1], Calc_Scalor(Calc_Penalty3(Master[i], Beginer[j]), 2), ClearDataArray());
                    DataArray dtemp2 = Calc_Add(Cost[i - 1, j], Calc_Penalty3(Master[i], Beginer[j]), ClearDataArray());
                    DataArray dtemp3 = Calc_Add(Cost[i, j - 1], Calc_Penalty3(Master[i], Beginer[j]), ClearDataArray());

                    Cost[i, j] = MinDataArraySet(dtemp1, dtemp2, dtemp3);
                }
            }
            distance = Calc_SumDataArray(Cost[M_FrameNumber - 1, B_FrameNumber - 1], M_FrameNumber) / (M_FrameNumber + B_FrameNumber);
            //Console.Write(Cost[M_FrameNumber - 1, B_FrameNumber - 1].X + "+" + Cost[M_FrameNumber - 1, B_FrameNumber - 1].Y + "+" + Cost[M_FrameNumber - 1, B_FrameNumber - 1].Z);
            //Console.WriteLine("/" + (M_FrameNumber + B_FrameNumber) + "=" + distance);
            return distance;
        }

        //ユークリッド距離計算関数
        private static float Calc_Penalty(DataArray A, DataArray B) {
            double X_dif, Y_dif, Z_dif;
            int xyzFreq = xFreq + yFreq + zFreq;
            X_dif = Math.Pow((A.X - B.X) * xyzFreq / xyzFreq, 2);
            Y_dif = Math.Pow((A.Y - B.Y) * xyzFreq / xyzFreq, 2);
            Z_dif = Math.Pow((A.Z - B.Z) * xyzFreq / xyzFreq, 2);
            return (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
        }

        //3つのデータから最小がどれかを探す関数
        private static int Min(float a, float b, float c) {
            if(a <= b && a <= c) {
                return 0;
            } else if(b <= c) {
                return 1;
            } else {
                return 2;
            }
        }

        //DP照合3 ユークリッド距離計算関数
        private static DataArray Calc_Penalty3(DataArray A, DataArray B) {
            DataArray dataArray = new DataArray {
                X = Math.Abs(A.X - B.X),
                Y = Math.Abs(A.Y - B.Y),
                Z = Math.Abs(A.Z - B.Z)
            };
            return dataArray;
        }

        //DP照合3 定数倍計算
        private static DataArray Calc_Scalor(DataArray A, int n) {
            DataArray dataArray = new DataArray {
                X = A.X * n,
                Y = A.Y * n,
                Z = A.Z * n
            };
            return dataArray;
        }

        //DP照合3 2つのDataArrayの加算
        private static DataArray Calc_Add(DataArray A, DataArray B, DataArray C) {
            DataArray dataArray = new DataArray {
                X = A.X + B.X + C.X,
                Y = A.Y + B.Y + C.Y,
                Z = A.Z + B.Z + C.Z
            };
            return dataArray;
        }

        private static DataArray MinDataArraySet(DataArray A, DataArray B, DataArray C) {
            DataArray dataArray = new DataArray {
                X = Min3(A.X, B.X, C.X),
                Y = Min3(A.Y, B.Y, C.Y),
                Z = Min3(A.Z, B.Z, C.Z),
            };
            return dataArray;
        }

        //3つのデータから最小がどれかを探す関数
        private static float Min3(float a, float b, float c) {
            if(a <= b && a <= c) {
                return a;
            } else if(b <= c) {
                return b;
            } else {
                return c;
            }
        }

        private static float Calc_SumDataArray(DataArray A, int n) {
            //return (A.X * xFreq  + A.Y * yFreq + A.Z * zFreq) / n;
            return A.X + A.Y + A.Z;
        }

        private static DataArray ClearDataArray() {
            DataArray dataArray = new DataArray {
                X = 0,
                Y = 0,
                Z = 0
            };
            return dataArray;
        }

        private static DataArray MaxValueDataArray() {
            DataArray dataArray = new DataArray {
                X = float.MaxValue,
                Y = float.MaxValue,
                Z = float.MaxValue
            };
            return dataArray;
        }

        //DataArray型構造体
        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }
    }
}