﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//3次元座標におけるDPマッチングのプログラム
namespace DPMatching2 {
    struct DataArray {
        public float X;
        public float Y;
        public float Z;
    }

    class DPMatching {
        const int DIAGONAL = 0; //斜め
        const int RIGHTUNDER = 1; //→↓
        const int UNDERRIGHT = 2; //↓→
        
        static void Main(string[] args) {
            int A_FrameNumber = 3; //パターンAのフレーム長
            int B_FrameNumber = 4; //パターンBのフレーム長
            DataArray[] A = new DataArray[A_FrameNumber]; //実際のデータ配列
            DataArray[] B = new DataArray[B_FrameNumber]; //実際のデータ配列
            int i,j;
            float Pena; //penalty
            float[,] Cost = new float[A_FrameNumber, B_FrameNumber]; 
            int[,] From = new int[A_FrameNumber, B_FrameNumber];
            float Distance; //パターン照合した結果どれだけ違うか

            //仮データ
            A[0].X = 1; A[0].Y = 1; A[0].Z = 1;
            A[1].X = 2; A[1].Y = 2; A[1].Z = 2;
            A[2].X = 3; A[2].Y = 3; A[2].Z = 3;
            B[0].X = 1; B[0].Y = 1; B[0].Z = 1;
            B[1].X = (float)7.0 / 4; B[1].Y = (float)7.0 / 4; B[1].Z = (float)7.0 / 4;
            B[2].X = 2; B[2].Y = 2; B[2].Z = 2;
            B[3].X = 3; B[3].Y = 3; B[3].Z = 3;

            //初期値
            Cost[0, 0] = Penalty(A[0], B[0]);

            //→方向に初期値設定
            for (i = 1; i < A_FrameNumber; i++) {
                Pena = Penalty(A[i],B[0]);
                Cost[i,0] = Cost[i-1,0] + Pena;
                From[i, 0] = RIGHTUNDER;
            }
            //↓方向に初期値設定
            for (j = 1; j < B_FrameNumber; j++) {
                Pena = Penalty(A[0], B[j]);
                Cost[0, j] = Cost[0, j - 1] + Pena;
                From[0, j] = UNDERRIGHT;
            }
            //コスト計算
            for (i = 1; i < A_FrameNumber; i++) {
                for (j = 1; j < B_FrameNumber; j++) {
                    float dtemp1 = Cost[i - 1, j - 1] + Penalty(A[i], B[j]) * 2;
                    float dtemp2 = Cost[i, j - 1] + Penalty(A[i], B[j]);
                    float dtemp3 = Cost[i - 1, j] + Penalty(A[i], B[j]);

                    //Console.Write(i + "," + j + " ");
                    int min = Min(dtemp1, dtemp2, dtemp3);
                    if (min == DIAGONAL) {
                        Cost[i, j] = dtemp1;
                        From[i, j] = DIAGONAL;
                    } else if (min == RIGHTUNDER) {
                        Cost[i, j] = dtemp2;
                        From[i, j] = RIGHTUNDER;
                    } else {
                        Cost[i, j] = dtemp3;
                        From[i, j] = UNDERRIGHT;
                    }
                }
            }
            Distance = Cost[A_FrameNumber - 1, B_FrameNumber - 1];
            Console.WriteLine(Distance);

            /*
            for (j = 0; j < B_FrameNumber; j++) {
                for (i = 0; i < A_FrameNumber; i++) {
                    Console.Write("{0} ", Cost[i,j]);
                }
                Console.WriteLine();
            }
            for (j = 0; j < B_FrameNumber; j++) {
                for (i = 0; i < A_FrameNumber; i++) {
                    Console.Write("{0} ", From[i, j]);
                }
                Console.WriteLine();
            }
            */

            i = A_FrameNumber - 1;
            j = B_FrameNumber - 1;
            while(i >= 0 && j >= 0) {
                //Console.WriteLine(i + " " + j);
                switch (From[i, j]) {
                    case 0:
                        i--;j--;
                        break;
                    case 1:
                        j--;
                        break;
                    case 2:
                        i--;
                        break;
                    default:
                        Console.WriteLine("error");
                        break;
                }                
            }
        }

        private static float Penalty(DataArray A, DataArray B) {
            double X_dif,Y_dif,Z_dif;
            float pena;
            X_dif = Math.Pow((A.X - B.X), 2);
            Y_dif = Math.Pow((A.Y - B.Y), 2);
            Z_dif = Math.Pow((A.Z - B.Z), 2);
            pena = (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
            return pena;
        }

        private static int Min(float a, float b, float c) {
            if (a <= b && a <= c) {
                return 0;
            } else if (b <= c) {
                return 1;
            } else {
                return 2;
            }
        }
    }
}
