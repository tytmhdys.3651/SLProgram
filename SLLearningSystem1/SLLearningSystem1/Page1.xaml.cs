﻿using AForge.Video.VFW;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SLLearningSystem1 {
    /// <summary>
    /// Page1.xaml の相互作用ロジック
    /// </summary>
    public partial class Page1 : Page {
        public Page1() {
            InitializeComponent();
        }
        
        const int NOTEPC = 1; // 撮影フレーム数の変更(2なら1/2になる)
        const int FRAMENUMBER = 150; // 30fps * 5s = 150frame
        const int RADIUS = 50; //表示円の半径
        System.Windows.Media.Brush RightHandLineBrush = System.Windows.Media.Brushes.Blue;
        System.Drawing.Color RightHandLineColor = System.Drawing.Color.Blue;

        //ページ遷移用
        NavigationService _navigation;

        //kinect接続関係
        KinectSensor kinect;
        MultiSourceFrameReader multiReader;
        FrameDescription colorFrameDesc;

        //画面表示用
        WriteableBitmap colorImage;
        byte[] colorBuffer;
        int colorStride;
        Int32Rect colorRect;
        ColorImageFormat colorFormat = ColorImageFormat.Bgra; 

        //骨格追跡用
        Body[] bodies;
        JointType[] jointType = new[] { JointType.SpineShoulder,
                                        JointType.HandRight,
                                        //JointType.HandLeft
                                        };

        //骨格位置の記録のための変数
        List<DataArray> SpineShoulder = new List<DataArray>();
        List<DataArray> HandRight = new List<DataArray>();
        //List<DataArray> HandLeft = new List<DataArray>();

        //軌跡描画用
        List<ColorSpacePoint> HandRightPoint = new List<ColorSpacePoint>();
        //List<ColorSpacePoint> HandLeftPoint = new List<ColorSpacePoint>();

        //座標保存用
        List<DataArray> R_MasterData3D = new List<DataArray>();
        //List<DataArray> L_MasterData3D = new List<DataArray>();

        //その他のグローバル変数
        int frame = 0;   //フレーム数
        bool on = false; //録画状態
        bool confirmCheck = false;
        bool testmode = false;
        bool StartCheck = false;
        DataArray MasterStartposition;
        //bool LeftHandCheck = false;

        //DataArray型構造体
        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        //ページロードしたら行う
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            _navigation = NavigationService; //画面遷移用変数初期化

            MasterStartposition = Word_to_MasterStartPosition(Wordname.Text);
            DrawStartEllipse(MasterStartposition);
            string filename = @"image\" + Wordname.Text + ".avi";
            Uri uri = new Uri(filename, UriKind.RelativeOrAbsolute);
            GoodModel.Source = uri;

            //kinectに接続できるかエラーチェック
            try {  
                //初期設定
                kinect = KinectSensor.GetDefault();
                kinect.Open();
                colorFrameDesc = kinect.ColorFrameSource.CreateFrameDescription(colorFormat);
                bodies = new Body[kinect.BodyFrameSource.BodyCount];

                //フレームリーダーを開く(データの読み込み準備)
                multiReader = kinect.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Color |
                    FrameSourceTypes.Body);
                multiReader.MultiSourceFrameArrived += MultiReader_MultiSourceFrameArrived;

                //画像枠作成
                colorImage = new WriteableBitmap(
                                    colorFrameDesc.Width, colorFrameDesc.Height,
                                    96, 96, PixelFormats.Bgra32, null); //外枠
                colorBuffer = new byte[colorFrameDesc.LengthInPixels * colorFrameDesc.BytesPerPixel];　//[1920*1080*4]
                colorRect = new Int32Rect(0, 0, colorFrameDesc.Width, colorFrameDesc.Height); 
                colorStride = colorFrameDesc.Width * (int)colorFrameDesc.BytesPerPixel; //1行の文字数(1920*4)   
                ImageColor.Source = colorImage;  //表示する
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        //複数のリーダーを同時に開く
        void MultiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            var multiFrame = e.FrameReference.AcquireFrame();
            if (multiFrame == null) {
                return;
            }

            UpdateColorFrame(multiFrame);
            UpdateBodyFrame(multiFrame);
        }

        //カラーフレームを取得
        private void UpdateColorFrame(MultiSourceFrame e) {           
            using (var colorFrame = e.ColorFrameReference.AcquireFrame()) {
                if (colorFrame == null) {
                    return;
                }               
                colorFrame.CopyConvertedFrameDataToArray(colorBuffer, colorFormat);  //colorデータを取得  
            }
        }

        //ボディフレームを取得
        private void UpdateBodyFrame(MultiSourceFrame e) {
            using (var bodyFrame = e.BodyFrameReference.AcquireFrame()) {
                if (bodyFrame == null) {
                    return;
                }         
                bodyFrame.GetAndRefreshBodyData(bodies);  //bodyデータを取得
                DrawFrame();
            }
        }

        private void DrawFrame() {
            colorImage.WritePixels(colorRect, colorBuffer, colorStride, 0);
            DrawBodyFrame();
            if (on) {
                if (frame % NOTEPC == 0) {
                    WriteableBitmap resizeImage;
                    resizeImage = colorImage.Resize(640, 360, WriteableBitmapExtensions.Interpolation.Bilinear); //リサイズ
                    SaveImage(resizeImage, @"image\image" + frame / NOTEPC + ".bmp");
                }
                frame++;
            }
        }

        //WriteableBitmapをbmpファイルとして保存
        private void SaveImage(WriteableBitmap bitmap, string fileName) {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }           
        }

        private void DrawBodyFrame() {
            CanvasBody.Children.Clear();
            //bodies[0-5]の中からisTrackedのものだけを対象とする
            foreach (var body in bodies.Where(b => b.IsTracked)) {
                //IReadOnlyDictionary<JointType,Joint> joint = body.Joints;
                foreach (var joint in body.Joints) {
                    for (int i = 0; i < jointType.Length; i++) {
                        if (joint.Value.JointType == jointType[i]) {
                            if (joint.Value.TrackingState == TrackingState.Tracked) {  //位置が追跡状態       
                                DrawEllipse(joint.Value, 10, System.Windows.Media.Brushes.Blue);
                            } else if (joint.Value.TrackingState == TrackingState.Inferred) {  //位置が推測状態
                                DrawEllipse(joint.Value, 10, System.Windows.Media.Brushes.Yellow);
                            } 
                        }
                    }
                }
            }
        }

        private void DrawEllipse(Joint joint, int R, System.Windows.Media.Brush brush) {
            var ellipse = new Ellipse() {
                Width = R,
                Height = R,
                Fill = brush,
            };

            //カメラ座標系をColor座標系に変換する
            var point = kinect.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
            if (point.X < 0 || point.Y < 0) {
                return;
            }

            //Color座標系で骨格位置を描画する
            Canvas.SetLeft(ellipse, point.X / 3 - (R / 2));
            Canvas.SetTop(ellipse, point.Y / 3 - (R / 2));
            CanvasBody.Children.Add(ellipse);          

            //位置の記録
            if (on) {
                DataArray tempData;
                tempData.X = joint.Position.X * 100;
                tempData.Y = joint.Position.Y * 100;
                tempData.Z = joint.Position.Z * 100;
                if (joint.JointType == jointType[0]) { //肩中央
                    SpineShoulder.Add(tempData);
                } else if (joint.JointType == jointType[1]) { //右手
                    HandRight.Add(tempData);                  
                }
            } else {
                if (joint.JointType == jointType[1]) {
                    if ((MasterStartposition.X - RADIUS < point.X / 3) && (point.X / 3 < MasterStartposition.X + RADIUS) && (MasterStartposition.Y - RADIUS < point.Y / 3) && (point.Y / 3 < MasterStartposition.Y + RADIUS)) {
                        confirmCheck = true;
                    } else {
                        confirmCheck = false;
                    }
                }
            }             
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e) {
            string filename = @"model\" + Wordname.Text;
            if (StartCheck == false) {
                if (testmode == false) {
                    if (confirmCheck == false) {
                        CountDown1.Text = "円の中にありません";
                        return;
                    }
                }
                SleepAsync();
            } else {
                StartCheck = false;
                return;
            }
        }

        private DataArray Word_to_MasterStartPosition(string Word) {
            int wordnumber = int.Parse(Word);
            DataArray[] dataArray = new DataArray[15];
            dataArray[0] = SetDataArray(300, 300, 0);
            return dataArray[wordnumber];
        }

        private void DrawStartEllipse(DataArray Position) {
            ConfirmCanvas.Children.Clear();
            var ellipse = new Ellipse() {
                Width = RADIUS * 2,
                Height = RADIUS * 2,
                StrokeThickness = 5,
                Stroke = RightHandLineBrush,
            };
            Canvas.SetLeft(ellipse, Position.X - RADIUS);
            Canvas.SetTop(ellipse, Position.Y - RADIUS);
            ConfirmCanvas.Children.Add(ellipse);
        }

        private void RecordButton_Click(object sender, RoutedEventArgs e) {
            testmode = !testmode;
            if(testmode == true) {
                ConfirmButton.Content = "テスト";
            } else {
                ConfirmButton.Content = "確認";
            }
        }

        //ボタン入力後の処理
        private async void SleepAsync() {
            float matchDistance;
            string B_filename = @"model\BeginerModel_R.txt";
            Page2 p2 = new Page2();
            StreamWriter sw;
            List<DataArray> Beginer = new List<DataArray>(); //初心者

            //録画開始
            on = true; StartCheck = true;
            CountDown1.Text = "Start";
            while(StartCheck == true) {
                await Task.Delay(1);
            }
            on = false;
            CountDown1.Text = "Stop";
            await Task.Delay(1);
        
            //骨格位置データ化→DP照合             
            if (BeginerDataSet(Beginer, true)) {
                matchDistance = (float)10.0 / DPmatching(R_MasterData3D, Beginer);  
                //p2.myName.Text += "　右手：" + matchDistance.ToString("F2") + Environment.NewLine;
                using(sw = new StreamWriter(B_filename, false, Encoding.Default)) {
                    for(int i = 0; i < Beginer.Count; i++) {
                        sw.WriteLine(Beginer[i].X + "," + Beginer[i].Y + "," + Beginer[i].Z + ",");
                    }
                }
            } else {
                matchDistance = -1;
            }

            AviChanger();
            if (matchDistance != -1) {
                p2.myName.Text += "　総合：" + matchDistance.ToString("F2") + Environment.NewLine;
            } else {
                p2.myName.Text += "　総合：エラー" + Environment.NewLine;
            }

            p2.hidden.Text = Wordname.Text;
            p2.myName.Text = Wordname.Text + " : " + Environment.NewLine;
            _navigation.Navigate(p2);
        }

        //ファイルから値を取り出す
        private void FileRead(string fileAdress, bool righthand) {
            const int ARRAYSIZE = 6;
            StreamReader sr;
            DataArray tmpData = new DataArray();
            float[] result = new float[ARRAYSIZE];

            R_MasterData3D.Clear();
            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            } catch {
                return;
            }

            while(sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string strBuffer = sr.ReadLine();
                String[] stResult = new String[ARRAYSIZE];
                for(int i = 0; i < ARRAYSIZE; i++) {
                    StringLength = strBuffer.IndexOf(",", StringLength); ;
                    stResult[i] = strBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    } catch(FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return;
                    } catch(OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }

                tmpData.X = result[0] - result[3];
                tmpData.Y = result[1] - result[4];
                tmpData.Z = result[2] - result[5];
                R_MasterData3D.Add(tmpData);
            }
            sr.Close();
        }

        //Kinectで取ったデータの処理
        private bool BeginerDataSet(List<DataArray> Beginer, bool Right) {
            DataArray tempData;
            List<DataArray> tempList = new List<DataArray>();
            int i = 0;      

            //エラー処理
            if (SpineShoulder.Count < 0) {
                return false;
            }


            //相対座標化
            for (i = 0; i < HandRight.Count; i++) {
                tempData.X = HandRight[i].X - SpineShoulder[i].X;
                tempData.Y = HandRight[i].Y - SpineShoulder[i].Y;
                tempData.Z = HandRight[i].Z - SpineShoulder[i].Z;
                tempList.Add(tempData);
            }

            //Beginer配列に代入
            Beginer.Add(ClearDataArray());  //(0,0,0)を加える
            for (i = 0; i < tempList.Count; i++) {
                Beginer.Add(tempList[i]);
            }
            return true;
        }

        //複数のbmpファイルをつなげてavi形式で動画化
        private void AviChanger() {
            int count = 0;
            int avicount = 0;
            string bmpfilename = @"image\image" + avicount + ".bmp";
            string avifilename = @"image\BeginerModel.avi";
            AVIWriter aviwriter = new AVIWriter();            
            
            System.Drawing.Pen pb = new System.Drawing.Pen(RightHandLineColor, 4);

            if (File.Exists(avifilename)) {
                File.Delete(avifilename);
            }

            aviwriter.Open(avifilename, 640, 360);
            while (File.Exists(bmpfilename)) {
                Bitmap bmp = new Bitmap(bmpfilename);
                Graphics g = Graphics.FromImage(bmp);
                if (HandRightPoint.Count != 0) {
                    for (int i = 0; i < HandRight.Count; i = i + NOTEPC) {
                        for(int j = 0; j < NOTEPC; j++) {
                            g.DrawLine(pb, HandRightPoint[i + j].X / 3, HandRightPoint[i + j].Y / 3, HandRightPoint[i + j + 1].X / 3, HandRightPoint[i + j + 1].Y / 3);
                        }
                    }
                }
                g.Dispose();
                aviwriter.AddFrame(bmp);
                bmp.Dispose();
                count = count + NOTEPC;
                avicount++;
                bmpfilename = @"image\image" + avicount + ".bmp";
            }

            count = 0;
            bmpfilename = @"image\image" + count + ".bmp";
            while (File.Exists(bmpfilename)) {
                File.Delete(bmpfilename);
                count++;
                bmpfilename = @"image\image" + count + ".bmp";
            }
            aviwriter.Close();
        }

        //DP照合
        private float DPmatching(List<DataArray> Master, List<DataArray> Beginer) {
            const int DIAGONAL = 0; //斜め
            const int RIGHTUNDER = 1; //→↓↓
            const int UNDERRIGHT = 2; //↓→→                                      
            int M_FrameNumber; //お手本のフレーム長
            int B_FrameNumber; //初心者手話のフレーム長
            int i, j;
            float distance = -1;

            M_FrameNumber = Master.Count;
            B_FrameNumber = Beginer.Count;

            float[,] Cost = new float[M_FrameNumber, B_FrameNumber];
            int[,] From = new int[M_FrameNumber, B_FrameNumber];
            int[,] c = new int[M_FrameNumber, B_FrameNumber];

            c[0, 0] = 0;
            Cost[0, 0] = 0;
            From[0, 0] = -1;

            //→方向に初期値設定
            for (i = 1; i < M_FrameNumber; i++) {
                c[i, 0] = 0;
                Cost[i, 0] = float.MaxValue;
                From[i, 0] = -1;
            }
            for (i = 1; i < M_FrameNumber; i++) {
                c[i, 1] = 2;
                Cost[i, 1] = Cost[i - 1, 0] + Calc_Penalty(Master[i], Beginer[1]) * 2;
                From[i, 1] = 0;
            }

            //↓方向に初期値設定
            for (j = 1; j < B_FrameNumber; j++) {
                c[0, j] = 0;
                Cost[0, j] = float.MaxValue;
                From[0, j] = -1;
            }
            for (j = 2; j < B_FrameNumber; j++) {
                c[1, j] = 2;
                Cost[1, j] = Cost[0, j - 1] + Calc_Penalty(Master[1], Beginer[j]) * 2;
                From[1, j] = 0;
            }

            //コスト計算
            for (i = 2; i < M_FrameNumber; i++) {
                for (j = 2; j < B_FrameNumber; j++) {
                    float dtemp1 = Cost[i - 1, j - 1] + Calc_Penalty(Master[i], Beginer[j]) * 2;
                    float dtemp2 = Cost[i - 2, j - 1] + Calc_Penalty(Master[i - 1], Beginer[j]) * 2 + Calc_Penalty(Master[i], Beginer[j]);
                    float dtemp3 = Cost[i - 1, j - 2] + Calc_Penalty(Master[i], Beginer[j - 1]) * 2 + Calc_Penalty(Master[i], Beginer[j]);

                    int min = Min(dtemp1, dtemp2, dtemp3);
                    if (min == DIAGONAL) {
                        c[i, j] = c[i - 1, j - 1] + 2;
                        Cost[i, j] = dtemp1;
                        From[i, j] = DIAGONAL;
                    } else if (min == RIGHTUNDER) {
                        c[i, j] = c[i - 2, j - 1] + 3;
                        Cost[i, j] = dtemp2;
                        From[i, j] = RIGHTUNDER;
                    } else {
                        c[i, j] = c[i - 1, j - 2] + 3;
                        Cost[i, j] = dtemp3;
                        From[i, j] = UNDERRIGHT;
                    }
                }
            }
            distance = Cost[M_FrameNumber - 1, B_FrameNumber - 1] / c[M_FrameNumber - 1, B_FrameNumber - 1];
            return distance;
        }

        //ユークリッド距離計算関数
        private static float Calc_Penalty(DataArray A, DataArray B) {
            double X_dif, Y_dif, Z_dif;
            X_dif = Math.Pow((A.X - B.X), 2);
            Y_dif = Math.Pow((A.Y - B.Y), 2);
            Z_dif = Math.Pow((A.Z - B.Z), 2);
            return (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
        }

        //原点からのユークリッド距離の差分を求める関数
        private static float Calc_Distance(DataArray A, DataArray B) {
            double X_dif = Math.Pow(A.X, 2);
            double Y_dif = Math.Pow(A.Y, 2);
            double Z_dif = Math.Pow(A.Z, 2);
            float A_Distance = (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
            X_dif = Math.Pow(B.X, 2);
            Y_dif = Math.Pow(B.Y, 2);
            Z_dif = Math.Pow(B.Z, 2);
            float B_Distance = (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
            return B_Distance - A_Distance;
        }

        //3つのデータから最小がどれかを探す関数
        private static int Min(float a, float b, float c) {
            if (a <= b && a <= c) {
                return 0;
            } else if (b <= c) {
                return 1;
            } else {
                return 2;
            }
        }

        //DataArray型変数の初期化用
        private DataArray ClearDataArray() {
            DataArray d;
            d.X = 0; d.Y = 0; d.Z = 0;
            return d;
        }

        private DataArray SetDataArray(float x, float y, float z) {
            DataArray dataArray;
            dataArray.X = x; dataArray.Y = y; dataArray.Z = z;
            return dataArray;
        }

        private void GoodModel_MediaEnded(object sender, RoutedEventArgs e) {
            GoodModel.Stop();
            PlayButtonG.Visibility = Visibility.Visible;
        }

        private void GoodModel_MouseDown(object sender, MouseButtonEventArgs e) {
            GoodModel.Pause();
            PlayButtonG.Visibility = Visibility.Visible;
        }

        private void PlayButtonG_Click(object sender, RoutedEventArgs e) {
            PlayButtonG.Visibility = Visibility.Hidden;
            GoodModel.Play();
        }
    }
}