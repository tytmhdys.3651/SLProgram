﻿using System.Windows;
using System.IO;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace SLLearningSystem1
{
    /// <summary>
    /// Title.xaml の相互作用ロジック
    /// </summary>
    public partial class Title : Page
    {
        public Title() {
            InitializeComponent();
        }

        NavigationService _navigation;

        private void StartButton_Click(object sender, RoutedEventArgs e) {
            _navigation = NavigationService;
            Page1 page1 = new Page1();
            page1.Wordname.Text = WordName.Text;
            string filename = @"model\" + WordName.Text + ".txt";
            if (File.Exists(filename) == false) {
                WordName.Text = "";
                return;
            }
            _navigation.Navigate(page1);
        }
    }
}