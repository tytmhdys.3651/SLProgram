﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace SLLearningSystem1 {
    /// <summary>
    /// Page2.xaml の相互作用ロジック
    /// </summary>
    public partial class Page2 : Page {
        public Page2() {
            InitializeComponent();
        }

        const int NOTEPC  = 1;
        NavigationService _navigation;

        //ページロードしてすぐ行う
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            _navigation = this.NavigationService;

            string filename = @"image\" + hidden.Text + ".avi";
            Uri uri = new Uri(filename, UriKind.RelativeOrAbsolute);
            GoodModel.Source = uri;

            BeginerModel.SpeedRatio = BeginerModel.SpeedRatio / NOTEPC;

            GoodModel.Play();
            GoodModel.Pause();
        }

        //"戻る"ボタンを押したとき
        private void BuckButton_Click(object sender, RoutedEventArgs e) {
            BeginerModel.Clock = null;
            BeginerModel.Close();
            string avifilename = @"image\BeginerModel.avi";

            if (File.Exists(avifilename)) {
                File.Delete(avifilename);
            }

            Page1 p1 = new Page1();
            _navigation.Navigate(p1);
        }

        //動画内をクリックしたとき
        private void BeginerModel_MouseDown(object sender, MouseButtonEventArgs e) {
            BeginerModel.Pause();
            PlayButtonB.Visibility = Visibility.Visible;
        }

        private void GoodModel_MouseDown(object sender, MouseButtonEventArgs e) {
            GoodModel.Pause();
            PlayButtonG.Visibility = Visibility.Visible;
        }

        //動画が終わったとき
        private void BeginerModel_MediaEnded(object sender, RoutedEventArgs e) {
            BeginerModel.Stop();
            PlayButtonB.Visibility = Visibility.Visible;
        }

        private void GoodModel_MediaEnded(object sender, RoutedEventArgs e) {
            GoodModel.Stop();
            PlayButtonG.Visibility = Visibility.Visible;
        }

        //▶ボタンを押したとき
        private void PlayButtonB_Click(object sender, RoutedEventArgs e) {
            blackbord.Visibility = Visibility.Hidden;
            PlayButtonB.Visibility = Visibility.Hidden;
            BeginerModel.Play();
        }

        private void PlayButtonG_Click(object sender, RoutedEventArgs e) {
            PlayButtonG.Visibility = Visibility.Hidden;
            GoodModel.Play();
        }

        private void PlayButtonBoth_Click(object sender, RoutedEventArgs e) {
            blackbord.Visibility = Visibility.Hidden;
            PlayButtonB.Visibility = Visibility.Hidden;
            PlayButtonG.Visibility = Visibility.Hidden;
            BeginerModel.Play();
            GoodModel.Play();
        }

        private void ZaxisButton_Click(object sender, RoutedEventArgs e) {
            var zaxis = new Zaxis();
            zaxis.wordname.Text = hidden.Text;
            zaxis.Show();
        }

        private void SlowCheck_Checked(object sender, RoutedEventArgs e) {
            BeginerModel.SpeedRatio = BeginerModel.SpeedRatio * 0.5;
            GoodModel.SpeedRatio = GoodModel.SpeedRatio * 0.5;
        }

        private void SlowCheck_Unchecked(object sender, RoutedEventArgs e) {
            BeginerModel.SpeedRatio = BeginerModel.SpeedRatio * 2;
            GoodModel.SpeedRatio = GoodModel.SpeedRatio * 2;
        }

        private void ReverseCheck_Checked(object sender, RoutedEventArgs e) {
            mirrorB.ScaleX = -1;
            mirrorG.ScaleX = -1;
        }

        private void ReverseCheck_Unchecked(object sender, RoutedEventArgs e) {
            mirrorB.ScaleX = 1;
            mirrorG.ScaleX = 1;           
        }
    }
}