﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SLLearningSystem1 {
    /// <summary>
    /// Zaxis.xaml の相互作用ロジック
    /// </summary>
    public partial class Zaxis : Window {
        public Zaxis() {
            InitializeComponent();
        }
        DispatcherTimer dispatcherTimer;
        List<DataArray> Beginer;
        int BeginerDatasetNumber;
        List<DataArray> Master;
        int MasterDatasetNumber;
        int timecheck;

        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            string M_filenameR = @"model\" + wordname.Text + "_R.txt";
            string B_filenameR = @"model\BeginerModel_R.txt";

            Beginer = new List<DataArray>();
            Master = new List<DataArray>();
            FileRead(Master, M_filenameR);           
            FileRead(Beginer, B_filenameR);
            MasterDatasetNumber = Master.Count;
            BeginerDatasetNumber = Beginer.Count;

            visSide();
        }

        private static bool FileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 3;
            StreamReader sr;
            DataArray tmpData;
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            }
            catch {
                return false;
            }

            while (sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for (i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    }
                    catch (FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    catch (OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }
                tmpData.X = result[0];
                tmpData.Y = result[1];
                tmpData.Z = result[2];
                list.Add(tmpData);
            }
            sr.Close();
            return true;
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e) {
            ButtonClickProcess();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal) {
                Interval = new TimeSpan(0, 0, 0, 0, 50)
            };
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Start();
        }

        private void SlowButton_Click(object sender, RoutedEventArgs e) {
            ButtonClickProcess();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal) {
                Interval = new TimeSpan(0, 0, 0, 0, 250)
            };
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Start();
        }

        private void ButtonClickProcess() {
            playButton.Click -= PlayButton_Click;
            slowButton.Click -= SlowButton_Click;
            radioButtonSet.IsEnabled = false;
            timecheck = 1;
            CanvasBody.Children.Clear();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e) {
            int max = Math.Max(BeginerDatasetNumber, MasterDatasetNumber);
            CanvasBody.Children.Clear();
            if (timecheck < max) {
                if (XZ.IsChecked == true) {
                    VisHead();
                    DrawEllipse(timecheck, true);
                } else if (YZ.IsChecked == true) {
                    visSide();
                    DrawEllipse(timecheck, false);
                }
            } else {
                dispatcherTimer.Stop();
                radioButtonSet.IsEnabled = true;
                playButton.Click += PlayButton_Click;
                slowButton.Click += SlowButton_Click;
            }
            timecheck++;
        }

        private void VisHead() {
            headLine.Visibility = Visibility.Visible;
            headEll.Visibility = Visibility.Visible;
            headPol.Visibility = Visibility.Visible;
            sideEll.Visibility = Visibility.Hidden;
            sidePol.Visibility = Visibility.Hidden;
            sideChair1.Visibility = Visibility.Hidden;
            sideChair2.Visibility = Visibility.Hidden;
        }
        private void visSide() {
            headLine.Visibility = Visibility.Hidden;
            headEll.Visibility = Visibility.Hidden;
            headPol.Visibility = Visibility.Hidden;
            sideEll.Visibility = Visibility.Visible;
            sidePol.Visibility = Visibility.Visible;
            sideChair1.Visibility = Visibility.Visible;
            sideChair2.Visibility = Visibility.Visible;
        }

        private void DrawEllipse(int i, bool X) {
            const double XTIMES = 1.5;
            //お手本
            var ellipseRM = SetEllipse(Brushes.Blue);
            var lineRM = SetLine(Brushes.Blue);

            //右手
            if (i < MasterDatasetNumber) {
                if (X) {
                    Canvas.SetLeft(ellipseRM, 100 - Master[i].Z * XTIMES - 2.5);
                    lineRM.X2 = 100 - Master[i].Z * XTIMES;
                    Canvas.SetTop(ellipseRM, 100 + Master[i].X * XTIMES - 5);
                    lineRM.Y1 = 135;
                    lineRM.Y2 = 100 + Master[i].X * XTIMES;
                } else {
                    Canvas.SetLeft(ellipseRM, 100 - Master[i].Z - 2.5);
                    lineRM.X2 = 100 - Master[i].Z;
                    Canvas.SetTop(ellipseRM, 100 - Master[i].Y - 5);
                    lineRM.Y1 = 120;
                    lineRM.Y2 = 100 - Master[i].Y;
                }
                CanvasBody.Children.Add(ellipseRM);
                CanvasBody.Children.Add(lineRM);
            }

            //ビギナー
            var ellipseRB = SetEllipse(Brushes.Red);
            var lineRB = SetLine(Brushes.Red);

            //右手
            if (i < BeginerDatasetNumber) {
                if (X) {
                    lineRB.X2 = 100 - Beginer[i].Z * 2;
                    Canvas.SetLeft(ellipseRB, 100 - Beginer[i].Z * 2 - 2.5);
                    Canvas.SetTop(ellipseRB, 100 + Beginer[i].X * 2 - 5.0);
                    lineRB.Y1 = 135;
                    lineRB.Y2 = 100 + Beginer[i].X * 2;
                } else {
                    lineRB.X2 = 100 - Beginer[i].Z;
                    Canvas.SetLeft(ellipseRB, 100 - Beginer[i].Z - 2.5);
                    Canvas.SetTop(ellipseRB, 100 - Beginer[i].Y - 5.0);
                    lineRB.Y1 = 120;
                    lineRB.Y2 = 100 - Beginer[i].Y;
                }
                CanvasBody.Children.Add(ellipseRB);
                CanvasBody.Children.Add(lineRB);
            }
        }

        private Ellipse SetEllipse(Brush brush) {
            Ellipse ell = new Ellipse {
                Width = 5,
                Height = 10,
                Stroke = brush
            };
            return ell;
        }

        private Line SetLine(Brush brush) {
            Line line = new Line {
                Stroke = brush,
                StrokeThickness = 1,
                X1 = 100
            };
            return line;
        }
    }
}