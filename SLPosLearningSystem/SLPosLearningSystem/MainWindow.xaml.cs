﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace SLLearningSystem {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {

        public MainWindow() {
            InitializeComponent();
        }

        KinectSensor kinect;
        MultiSourceFrameReader multiReader;
        FrameDescription depthFrameDesc;

        //表示用
        WriteableBitmap depthImage;
        ushort[] depthBuffer;
        byte[] depthBitmapBuffer;
        int depthStride;
        Int32Rect depthRect;

        Body[] bodies;
        JointType[] jointType = new[] {//JointType.ElbowLeft,JointType.ElbowRight,JointType.HandLeft,
                                       JointType.HandRight,
                                       //JointType.HandTipLeft,JointType.HandTipRight,
                                       //JointType.Head,JointType.Neck,
                                       //JointType.ShoulderLeft,JointType.ShoulderRight,
                                       //JointType.SpineShoulder, 
                                       //JointType.SpineMid,
                                       //JointType.WristLeft,JointType.WristRight
                                       };
        int jointTypeNumber;

        StreamWriter writer = new StreamWriter(@"C:\SLTest\Test.txt", false, Encoding.GetEncoding("Shift_Jis"));
        StreamWriter resultWrite = new StreamWriter(@"C:\SLTest\Result.txt", false, Encoding.GetEncoding("Shift_Jis"));
        int a = 0; //骨格追跡後の改行用
        Boolean on = false;
        string searchWord;

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            try {
                jointTypeNumber = jointType.Length;

                //kinectと接続                
                kinect = KinectSensor.GetDefault();
                kinect.Open();

                depthFrameDesc = kinect.DepthFrameSource.FrameDescription;

                //bodyを入れる配列を作る
                bodies = new Body[kinect.BodyFrameSource.BodyCount];

                //フレームリーダーを開く(データの読み込み準備)
                multiReader = kinect.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Depth |
                    FrameSourceTypes.Body);
                multiReader.MultiSourceFrameArrived += multiReader_MultiSourceFrameArrived;

                depthImage = new WriteableBitmap(
                                    depthFrameDesc.Width, depthFrameDesc.Height,
                                    96, 96, PixelFormats.Gray8, null); //外枠
                depthBuffer = new ushort[depthFrameDesc.LengthInPixels];
                depthBitmapBuffer = new byte[depthFrameDesc.LengthInPixels];
                depthRect = new Int32Rect(0, 0, depthFrameDesc.Width, depthFrameDesc.Height);
                depthStride = (int)(depthFrameDesc.Width); //1行の文字数   

                ImageColor.Source = depthImage;
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
                Close();
            }
        }

        void multiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            var multiFrame = e.FrameReference.AcquireFrame();
            if (multiFrame == null) {
                return;
            }

            UpdateDepthFrame(multiFrame);
            UpdateBodyFrame(multiFrame);

            DrawDepthFrame();
        }

        private void UpdateDepthFrame(MultiSourceFrame e) {
            //深度フレームを取得
            using (var depthFrame = e.DepthFrameReference.AcquireFrame()) {
                if (depthFrame == null) {
                    return;
                }

                //depthデータを取得
                depthFrame.CopyFrameDataToArray(depthBuffer);
            }
        }

        private void UpdateBodyFrame(MultiSourceFrame e) {
            using (var bodyFrame = e.BodyFrameReference.AcquireFrame()) {
                if (bodyFrame == null) {
                    return;
                }

                //bodyデータを取得
                bodyFrame.GetAndRefreshBodyData(bodies);
            }
        }

        private void DrawDepthFrame() {
            DrawFrame();

            for (int i = 0; i < depthBuffer.Length; i++) {
                depthBitmapBuffer[i] = (byte)(depthBuffer[i] % 255);
            }
            depthImage.WritePixels(depthRect, depthBitmapBuffer, depthStride, 0);
        }

        private void DrawFrame() {
            //追跡しているBodyのみループする
            foreach (var body in bodies.Where(b => b.IsTracked)) {
                foreach (var joint in body.Joints) {
                    for (int i = 0; i < jointType.Length; i++) {
                        if (joint.Value.JointType == jointType[i]) {
                            if (joint.Value.TrackingState == TrackingState.Tracked) {  //手の位置が追跡状態
                                if (!on) {
                                    on = true;
                                }
                                DrawEllipse(joint.Value, 5, Brushes.Blue);
                            } else if (joint.Value.TrackingState == TrackingState.Inferred) {  //手の位置が推測状態
                                DrawEllipse(joint.Value, 5, Brushes.Yellow);
                            }
                        }
                    }
                }
            }
        }

        private void DrawEllipse(Joint joint, int R, Brush brush) {
            var ellipse = new Ellipse() {
                Width = R,
                Height = R,
                Fill = brush,
            };

            //カメラ座標系をColor座標系に変換する
            var point = kinect.CoordinateMapper.MapCameraPointToDepthSpace(joint.Position);
            if (point.X < 0 || point.Y < 0) {
                return;
            }

            //Color座標系で円を配置する
            Canvas.SetLeft(ellipse, point.X - (R / 2));
            Canvas.SetTop(ellipse, point.Y - (R / 2));
            CanvasBody.Children.Add(ellipse);

            if (on) {
                //writer.Write("{0},{1},{2},",joint.Position.X * 100, joint.Position.Y * 100, joint.Position.Z * 100);
                a++;
                if (a % jointTypeNumber == 0) {
                    //writer.Write(Environment.NewLine);
                }
            }
        }

        const int FILENAMEARRAY = 12;

        private void SearchButton_Click(object sender, RoutedEventArgs e) {
            int i = -1, j = -1;
            CanvasBody.Children.Clear();
            searchWord = Textbox1.Text;
            if (searchWord.Length > 1) {
                i = int.Parse(searchWord.Substring(0, 1));
                j = int.Parse(searchWord.Substring(1, 1));
            }
            Test.Text = searchWord + Environment.NewLine;
            Test.Text = ShowData(DPmatching(i, j), Test.Text);
        }

        private void TwiceButton_Click(object sender, RoutedEventArgs e) {
            for (int i = 0; i < FILENAMEARRAY; i++) {
                for (int j = 0; j < FILENAMEARRAY; j++) {
                    resultWrite.Write("{0:f4} ", DPmatching(i, j).Distance);
                }
                resultWrite.Write(Environment.NewLine);
            }
        }

        struct DataIndex {
            public float Distance;
            public int i;
            public int j;
            public int A;
            public int B;
        }

        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        private string ShowData(DataIndex dataIndex, string p) {
            p += dataIndex.Distance.ToString() + " ";
            p += dataIndex.i.ToString() + " ";
            p += dataIndex.j.ToString() + " ";
            p += dataIndex.A.ToString() + " ";
            p += dataIndex.B.ToString() + " ";
            return p;
        }

        private void showDataClear(DataIndex dataindex) {
            dataindex.Distance = -1;
            dataindex.i = -1;
            dataindex.j = -1;
            dataindex.A = -1;
            dataindex.B = -1;
        }

        private DataIndex DPmatching(int m, int n) {
            const int DIAGONAL = 0; //斜め
            const int RIGHTUNDER = 1; //→↓↓
            const int UNDERRIGHT = 2; //↓→→              
            int A_FrameNumber; //パターンAのフレーム長
            int B_FrameNumber; //パターンBのフレーム長
            List<DataArray> A = new List<DataArray>(); //初心者
            List<DataArray> B = new List<DataArray>(); //お手本
            int i, j;
            float distance;
            int MinI, MinJ;
            string[] filename = new string[FILENAMEARRAY];
            DataIndex showdata = new DataIndex();

            showDataClear(showdata);

            if (m < 0 || m >= FILENAMEARRAY || n < 0 || n >= FILENAMEARRAY) {
                return showdata;
            }

            filename[0] = @"C:\SLTest\go\handseg\go1.txt";
            filename[1] = @"C:\SLTest\go\handseg\go2.txt";
            filename[2] = @"C:\SLTest\go\handseg\go3.txt";
            filename[3] = @"C:\SLTest\go\handseg\go4.txt";
            filename[4] = @"C:\SLTest\go\handseg\come1a.txt";
            filename[5] = @"C:\SLTest\go\handseg\come2.txt";
            filename[6] = @"C:\SLTest\go\handseg\shortgo.txt";
            filename[7] = @"C:\SLTest\go\handseg\go5.txt";
            filename[8] = @"C:\SLTest\go\handseg\roopgo.txt";
            filename[9] = @"C:\SLTest\go\handseg\see1.txt";
            filename[10] = @"C:\SLTest\go\handseg\see2.txt";
            filename[11] = @"C:\SLTest\go\handseg\say.txt";
   
         
            fileRead(A, filename[m]);
            //Calc_Nom(A);
            //Calc_MotData(A);
            A_FrameNumber = A.Count;

            fileRead(B, filename[n]);            
            //Calc_Nom(B);            
            //Calc_MotData(B);
            B_FrameNumber = B.Count;

            float[,] Cost = new float[A_FrameNumber, B_FrameNumber];
            int[,] From = new int[A_FrameNumber, B_FrameNumber];
            int[,] c = new int[A_FrameNumber, B_FrameNumber];

            c[0, 0] = 0;
            Cost[0, 0] = 0;
            From[0, 0] = -1;

            //→方向に初期値設定
            for (i = 1; i < A_FrameNumber; i++) {
                c[i, 0] = 0;
                Cost[i, 0] = float.MaxValue;
                From[i, 0] = -1;
            }
            for (i = 1; i < A_FrameNumber; i++) {
                c[i, 1] = 2;
                Cost[i, 1] = Cost[i - 1, 0] + Calc_Penalty(A[i], B[1]) * 2;
                From[i, 1] = 0;
            }

            //↓方向に初期値設定
            for (j = 1; j < B_FrameNumber; j++) {
                c[0, j] = 0;
                Cost[0, j] = float.MaxValue;
                From[0, j] = -1;
            }
            for (j = 2; j < B_FrameNumber; j++) {
                c[1, j] = 2;
                Cost[1, j] = Cost[0, j - 1] + Calc_Penalty(A[1], B[j]) * 2;
                From[1, j] = 0;
            }

            //コスト計算
            for (i = 2; i < A_FrameNumber; i++) {
                for (j = 2; j < B_FrameNumber; j++) {
                    float dtemp1 = Cost[i - 1, j - 1] + Calc_Penalty(A[i], B[j]) * 2;
                    float dtemp2 = Cost[i - 2, j - 1] + Calc_Penalty(A[i - 1], B[j]) * 2 + Calc_Penalty(A[i], B[j]);
                    float dtemp3 = Cost[i - 1, j - 2] + Calc_Penalty(A[i], B[j - 1]) * 2 + Calc_Penalty(A[i], B[j]);
                    //writer.WriteLine("{0} {1} : {2} {3} {4}", i, j, dtemp1, dtemp2, dtemp3);

                    int min = Min(dtemp1, dtemp2, dtemp3);
                    if (min == DIAGONAL) {
                        c[i, j] = c[i - 1, j - 1] + 2;
                        Cost[i, j] = dtemp1;
                        From[i, j] = DIAGONAL;
                    } else if (min == RIGHTUNDER) {
                        c[i, j] = c[i - 2, j - 1] + 3;
                        Cost[i, j] = dtemp2;
                        From[i, j] = RIGHTUNDER;
                    } else {
                        c[i, j] = c[i - 1, j - 2] + 3;
                        Cost[i, j] = dtemp3;
                        From[i, j] = UNDERRIGHT;
                    }
                }
            }

            MinI = A_FrameNumber - 1;
            MinJ = B_FrameNumber - 1;
            distance = Cost[MinI, MinJ] / c[MinI, MinJ];
            for (i = A_FrameNumber - 1; i > 9 * A_FrameNumber / 10; i--) {
                for (j = B_FrameNumber - 1; j > 9 * B_FrameNumber / 10; j--) {
                    if (distance > Cost[i, j] / c[i, j]) {
                        distance = Cost[i, j] / c[i, j];
                        MinI = i; MinJ = j;
                    }
                }
            }
            
            while (MinI > 0 && MinJ > 0) {
                switch (From[MinI, MinJ]) {
                    case DIAGONAL:
                        MinI--; MinJ--;
                        break;
                    case RIGHTUNDER:
                        MinI = MinI - 2;
                        MinJ = MinJ - 1;
                        break;
                    case UNDERRIGHT:
                        MinI = MinI - 1;
                        MinJ = MinJ - 2;
                        break;
                    default:
                        return showdata;
                }
            }

            //writer.Write("{0} {1} {2} {3} {4}", distance, A_FrameNumber, B_FrameNumber, i, j);
            showdata.Distance = distance;
            showdata.i = MinI;
            showdata.j = MinJ;
            showdata.A = i;
            showdata.B = j;
            return showdata;
        }

        //ユークリッド距離計算関数(ペナルティ)
        private static float Calc_Penalty(DataArray A, DataArray B) {
            double X_dif, Y_dif, Z_dif;
            X_dif = Math.Pow((A.X - B.X), 2);
            Y_dif = Math.Pow((A.Y - B.Y), 2);
            Z_dif = Math.Pow((A.Z - B.Z), 2);
            return (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
        }

        //3つのデータから最小がどれかを探す関数
        private static int Min(float a, float b, float c) {
            if (a <= b && a <= c) {
                return 0;
            } else if (b <= c) {
                return 1;
            } else {
                return 2;
            }
        }

        //平均値を返す関数
        private static DataArray Calc_Average(List<DataArray> A) {
            DataArray ResultAverage;
            float Xave = 0; float Yave = 0; float Zave = 0;
            foreach (DataArray D in A) {
                Xave = Xave + D.X;
                Yave = Yave + D.Y;
                Zave = Zave + D.Z;
            }
            ResultAverage.X = Xave / (A.Count - 1);
            ResultAverage.Y = Yave / (A.Count - 1);
            ResultAverage.Z = Zave / (A.Count - 1);
            return ResultAverage;
        }

        //標準偏差を返す関数
        private static DataArray Calc_SD(List<DataArray> A, DataArray Average) {
            DataArray ResultSD;
            Double XSD = 0; Double YSD = 0; Double ZSD = 0;
            foreach (DataArray D in A) {
                XSD = XSD + Math.Pow(D.X - Average.X, 2);
                YSD = YSD + Math.Pow(D.Y - Average.Y, 2);
                ZSD = ZSD + Math.Pow(D.Z - Average.Z, 2);
            }
            ResultSD.X = (float)Math.Sqrt(XSD / (A.Count - 1));
            ResultSD.Y = (float)Math.Sqrt(YSD / (A.Count - 1));
            ResultSD.Z = (float)Math.Sqrt(ZSD / (A.Count - 1));
            return ResultSD;
        }

        //標準化
        private static DataArray Calc_Data(List<DataArray> A) {
            DataArray Average = Calc_Average(A);
            DataArray SDivision = Calc_SD(A, Average);
            DataArray TmpData;
            int i;

            for (i = 1; i < A.Count;i++ ) {
                TmpData.X = (A[i].X - Average.X) / SDivision.X;
                TmpData.Y = (A[i].Y - Average.Y) / SDivision.Y;
                TmpData.Z = (A[i].Z - Average.Z) / SDivision.Z;
                A[i] = TmpData;
            }
            return Average;
        }

        //動きデータへ
        private static void Calc_MotData(List<DataArray> A) {
            DataArray TmpData;
            int i;
            for (i = 2; i < A.Count; i++) {
                TmpData.X = A[i].X - A[i - 1].X;
                TmpData.Y = A[i].Y - A[i - 1].Y;
                TmpData.Z = A[i].Z - A[i - 1].Z;
                A[i - 1] = TmpData;
            }
            A.Remove(A[i - 1]);
        }

        //最初の値を1とする正規化
        private static void Calc_Nom(List<DataArray> A) {
            DataArray TmpData = DataArray_Clear();
            DataArray NomData = A[0];
            float distance;

            A.Add(TmpData);
            for (int i = 0; i < A.Count - 1; i++) {
                distance = (float)Math.Sqrt(Math.Pow(NomData.X, 2) + Math.Pow(NomData.Y, 2) + Math.Pow(NomData.Z, 2));
                TmpData.X = NomData.X / distance;
                TmpData.Y = NomData.Y / distance;
                TmpData.Z = NomData.Z / distance;
                NomData = A[i + 1];
                A[i + 1] = TmpData;
            }
            A[0] = DataArray_Clear();
        }

        private static DataArray DataArray_Clear() {
            DataArray tmpData;
            tmpData.X = 0; tmpData.Y = 0; tmpData.Z = 0;
            return tmpData;
        }

        private static void fileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 6;
            StreamReader sr;
            DataArray tmpData = new DataArray();            
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            }
            catch (FileNotFoundException ex) {
                Console.WriteLine(ex.Message);
                return;
            }

            while (sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for (i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    }
                    catch (FormatException ex) {
                        Console.WriteLine(ex.Message);
                    }
                    catch (OverflowException ex) {
                        Console.WriteLine(ex.Message);
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }

                tmpData.X = result[0] - result[3];
                tmpData.Y = result[1] - result[4];
                tmpData.Z = result[2] - result[5];
                list.Add(tmpData);
            }            
            sr.Close();
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            if (multiReader != null) {
                multiReader.Dispose();
                multiReader = null;
            }

            writer.Close();
            resultWrite.Close();

            if (kinect != null) {
                kinect.Close();
                kinect = null;
            }
        }
    }
}