﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SLLearningSystem1 {
    /// <summary>
    /// Zaxis.xaml の相互作用ロジック
    /// </summary>
    public partial class Zaxis : Window {
        public Zaxis() {
            InitializeComponent();
        }

        DispatcherTimer dispatcherTimer;
        List<DataArray> Beginer;
        int BeginerDatasetNumber;
        List<DataArray> Master;
        int MasterDatasetNumber;
        List<DataArray> Beginer_E;
        List<DataArray> Master_E;
        int timecheck;
        Brush BeginerColor = Brushes.Blue;

        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            string M_filenameR = @"model\" + wordname.Text + "_R_hand.txt";
            string B_filenameR = @"model\BeginerModel_R.txt";
            string M_filenameR_E = @"model\" + wordname.Text + "_R_elbow.txt";
            string B_filenameR_E = @"model\BeginerModel_R_elbow.txt";

            Beginer = new List<DataArray>();
            Master = new List<DataArray>();
            Beginer_E = new List<DataArray>();
            Master_E = new List<DataArray>();
            FileRead(Master, M_filenameR);           
            FileRead(Beginer, B_filenameR);
            FileRead(Master_E, M_filenameR_E);
            FileRead(Beginer_E, B_filenameR_E);
            MasterDatasetNumber = Math.Max(Master.Count,Master_E.Count);
            BeginerDatasetNumber = Math.Max(Beginer.Count,Beginer_E.Count);
            int maxvalue = Math.Max(MasterDatasetNumber, BeginerDatasetNumber);
            if (Master.Count < maxvalue) {
                for (int i = Master.Count - 1; i < maxvalue; i++) {
                    Master.Add(Master[i]);
                }
            }
            if (Master_E.Count < maxvalue) {
                for (int i = Master_E.Count - 1; i < maxvalue; i++) {
                    Master_E.Add(Master_E[i]);
                }
            }
            if (Beginer.Count < maxvalue) {
                for (int i = Beginer.Count - 1; i < maxvalue; i++) {
                    Beginer.Add(Beginer[i]);
                }
            }
            if (Beginer_E.Count < maxvalue) {
                for (int i = Beginer_E.Count - 1; i < maxvalue; i++) {
                    Beginer_E.Add(Beginer_E[i]);
                }
            }
        }

        private static bool FileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 3;
            StreamReader sr;
            DataArray tmpData;
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            }
            catch {
                return false;
            }

            while (sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for (i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    }
                    catch (FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    catch (OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }
                tmpData.X = result[0];
                tmpData.Y = result[1];
                tmpData.Z = result[2];
                list.Add(tmpData);
            }
            sr.Close();
            return true;
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e) {
            PlayReady();
        }

        private void PlayReady() {
            ButtonClickProcess();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal) {
                Interval = new TimeSpan(0, 0, 0, 0, 50)
            };
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Start();
        }

        private void SlowButton_Click(object sender, RoutedEventArgs e) {
            ButtonClickProcess();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal) {
                Interval = new TimeSpan(0, 0, 0, 0, 100)
            };
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Start();
        }

        private void ButtonClickProcess() {
            playButton.Click -= PlayButton_Click;
            slowButton.Click -= SlowButton_Click;
            timecheck = 1;
            CanvasBody.Children.Clear();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e) {
            int max = Math.Max(BeginerDatasetNumber, MasterDatasetNumber);
            CanvasBody.Children.Clear();
            if (timecheck < max) {
                DrawEllipse(timecheck);
            } else {
                dispatcherTimer.Stop();
                playButton.Click += PlayButton_Click;
                slowButton.Click += SlowButton_Click;
            }
            timecheck++;
        }

        private void DrawEllipse(int i) {
            const double XTIMES = 2;
            //お手本
            var ellipseRM_H = SetEllipse(Brushes.Red);
            var lineRM_EL = SetLine(Brushes.Red);
            var lineRM_ER = SetLine(Brushes.Red);
            var lineRM_HT = SetLine(Brushes.Red);
            var lineRM_HU = SetLine(Brushes.Red);

            //右手
            if (i < MasterDatasetNumber) {
                Canvas.SetLeft(ellipseRM_H, 83 - Master[i].Z * XTIMES - 15);
                Canvas.SetTop(ellipseRM_H, 97 - Master[i].Y * XTIMES - 15);
                lineRM_EL.X1 = 68;
                lineRM_EL.Y1 = 110;
                lineRM_EL.X2 = 83 - Master_E[i].Z * XTIMES;
                lineRM_EL.Y2 = 97 - Master_E[i].Y * XTIMES + 15;
                lineRM_ER.X1 = 98;
                lineRM_ER.Y1 = 110;
                lineRM_ER.X2 = 83 - Master_E[i].Z * XTIMES;
                lineRM_ER.Y2 = 97 - Master_E[i].Y * XTIMES - 15;
                lineRM_HT.X1 = 83 - Master_E[i].Z * XTIMES;
                lineRM_HT.Y1 = 97 - Master_E[i].Y * XTIMES - 15;
                lineRM_HT.X2 = 83 - Master[i].Z * XTIMES - 15;
                lineRM_HT.Y2 = 97 - Master[i].Y * XTIMES;
                lineRM_HU.X1 = 83 - Master_E[i].Z * XTIMES;
                lineRM_HU.Y1 = 97 - Master_E[i].Y * XTIMES + 15;
                lineRM_HU.X2 = 83 - Master[i].Z * XTIMES;
                lineRM_HU.Y2 = 97 - Master[i].Y * XTIMES + 15;
                CanvasBody.Children.Add(ellipseRM_H);
                CanvasBody.Children.Add(lineRM_EL);
                CanvasBody.Children.Add(lineRM_ER);
                CanvasBody.Children.Add(lineRM_HT);
                CanvasBody.Children.Add(lineRM_HU);
            }

            //ビギナー
            var ellipseRB_H = SetEllipse(BeginerColor);
            var lineRB_EL = SetLine(BeginerColor);
            var lineRB_ER = SetLine(BeginerColor);
            var lineRB_HT = SetLine(BeginerColor);
            var lineRB_HU = SetLine(BeginerColor);

            //右手
            if (i < BeginerDatasetNumber) {
                Canvas.SetLeft(ellipseRB_H, 83 - Beginer[i].Z * XTIMES - 15);
                Canvas.SetTop(ellipseRB_H, 97 - Beginer[i].Y * XTIMES - 15);
                lineRB_EL.X1 = 68;
                lineRB_EL.Y1 = 110;
                lineRB_EL.X2 = 83 - Beginer_E[i].Z * XTIMES;
                lineRB_EL.Y2 = 97 - Beginer_E[i].Y * XTIMES + 15;
                lineRB_ER.X1 = 98;
                lineRB_ER.Y1 = 110;
                lineRB_ER.X2 = 83 - Beginer_E[i].Z * XTIMES;
                lineRB_ER.Y2 = 97 - Beginer_E[i].Y * XTIMES - 15;
                lineRB_HT.X1 = 83 - Beginer_E[i].Z * XTIMES;
                lineRB_HT.Y1 = 97 - Beginer_E[i].Y * XTIMES - 15;
                lineRB_HT.X2 = 83 - Beginer[i].Z * XTIMES - 15;
                lineRB_HT.Y2 = 97 - Beginer[i].Y * XTIMES;
                lineRB_HU.X1 = 83 - Beginer_E[i].Z * XTIMES;
                lineRB_HU.Y1 = 97 - Beginer_E[i].Y * XTIMES + 15;
                lineRB_HU.X2 = 83 - Beginer[i].Z * XTIMES;
                lineRB_HU.Y2 = 97 - Beginer[i].Y * XTIMES + 15;
                CanvasBody.Children.Add(ellipseRB_H);
                CanvasBody.Children.Add(lineRB_EL);
                CanvasBody.Children.Add(lineRB_ER);
                CanvasBody.Children.Add(lineRB_HT);
                CanvasBody.Children.Add(lineRB_HU);
            }
        }

        private Ellipse SetEllipse(Brush brush) {
            Ellipse ell = new Ellipse {
                Width = 30,
                Height = 30,
                Stroke = brush
            };
            return ell;
        }

        private Line SetLine(Brush brush) {
            Line line = new Line {
                Stroke = brush,
                StrokeThickness = 1,
            };
            return line;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e) {
            Close();
        }

        private void Window_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            Close();
        }

        private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            PlayReady();
        }
    }
}