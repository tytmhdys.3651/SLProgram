﻿using System.Windows;
using System.IO;
using System.Text;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace SLLearningSystem1
{
    /// <summary>
    /// Title.xaml の相互作用ロジック
    /// </summary>
    public partial class Title : Page
    {
        public Title() {
            InitializeComponent();
        }

        NavigationService _navigation;

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            for(int i = 0; i < 16; i++) {
                string resultfilename = @"result\" + i.ToString() + "_C.txt";
                bool x = File.Exists(resultfilename);
                if (File.Exists(resultfilename) == true) {
                    switch (i) {
                        case 0:
                            word_0.IsEnabled = false;
                            //word_0.Content = "行く";
                            break;
                        case 1:
                            word_1.IsEnabled = false;
                            //word_1.Content = "美しい";
                            break;
                        case 2:
                            word_2.IsEnabled = false;
                            //word_2.Content = "息子";
                            break;
                        case 3:
                            word_3.IsEnabled = false;
                            //word_3.Content = "買う";
                            break;
                        case 4:
                            word_4.IsEnabled = false;
                            //word_4.Content = "遠い";
                            break;
                        case 5:
                            word_5.IsEnabled = false;
                            //word_5.Content = "売る";
                            break;
                        case 6:
                            word_6.IsEnabled = false;
                            //word_6.Content = "こんにちは";
                            break;
                        case 7:
                            word_7.IsEnabled = false;
                            //word_7.Content = "北";
                            break;
                        case 8:
                            word_8.IsEnabled = false;
                            //word_8.Content = "涼しい";
                            break;
                        case 9:
                            word_9.IsEnabled = false;
                            //word_9.Content = "毎日";
                            break;
                        case 10:
                            word_10.IsEnabled = false;
                            //word_10.Content = "日曜日";
                            break;
                    }                 
                }
            }
        }

        private void ButtonClickAction(string ButtonNumber, string filenumber, string Wordname) {
            _navigation = NavigationService;
            Page1 page1 = new Page1();
            //page1.Wordname.Text = Wordname;
            page1.HiddenWordname.Text = filenumber;
            string resultfilename = @"result\" + ButtonNumber + "_C.txt";
            using (StreamWriter streamWriter = new StreamWriter(resultfilename, false, Encoding.Default)) {
            }
            resultfilename = @"result\" + filenumber + ".txt";
            using (StreamWriter streamWriter = new StreamWriter(resultfilename, true, Encoding.Default)) {
            }

            _navigation.Navigate(page1);
        }

        private void zero_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("0", "0", "行く");
        }

        private void one_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("1","3", "美しい");
        }

        private void two_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("2","4", "息子");
        }

        private void three_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("3", "6", "毎日");
        }

        private void four_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("4", "9", "遠い");
        }

        private void five_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("5", "15", "売る");
        }

        private void six_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("6", "1", "こんにちは");
        }

        private void seven_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("7", "16", "一日");
        }

        private void eight_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("8", "14", "涼しい");
        }

        private void nine_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("9", "8", "買う");           
        }

        private void ten_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("10", "17", "毎月");
        }

        private void eleven_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("11", "5", "昨日");
        }

        private void twelve_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("12", "10", "早い");
        }

        private void thirteen_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("13", "11", "おはよう");
        }

        private void fourteen_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("14", "12", "原");
        }

        private void fifteen_Click(object sender, RoutedEventArgs e) {
            ButtonClickAction("15", "13", "手話");
        }
    }
}