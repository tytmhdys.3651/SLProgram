﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SLLearningSystem1 {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            _navi = this.myFrame.NavigationService;
        }

        private NavigationService _navi;
        private List<Uri> _uriList = new List<Uri>() {
            new Uri("Title.xaml",UriKind.Relative),
            new Uri("Page1.xaml",UriKind.Relative),
            new Uri("Page2.xaml",UriKind.Relative)
        };

        private void myFrame_Loaded(object sender, RoutedEventArgs e) {
            if(File.Exists("image\\BeginerModel.avi")) {
                File.Delete("image\\BeginerModel.avi");
            }

            _navi.Navigate(_uriList[0]);
        }

        //private void window_closed(object sender, eventargs e) {
        //    int filenumber = 0;
        //    string filename = @"image\test" + filenumber + ".bmp";
        //    while (file.exists(filename)) {
        //        file.delete(filename);
        //        filenumber++;
        //        filename = @"image\test" + filenumber + ".bmp";
        //    }
        //}
         
        private void Window_Closed(object sender, EventArgs e) {           
            string filename = @"image\BeginerModel.avi";
            if (File.Exists(filename)) {
                File.Delete(filename);
            }
        }
    }    
}
