﻿using AForge.Video.VFW;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SLLearningSystem1 {
    /// <summary>
    /// Page1.xaml の相互作用ロジック
    /// </summary>
    public partial class Page1 : Page {
        public Page1() {
            InitializeComponent();
        }

        const int NOTEPC = 2; // 撮影フレーム数の変更(2なら1/2になる)
        const int DISTANCE = 5;
        const int FRAMENUMBER = 150; // 30fps * 5s = 150frame
        const int RADIUS = 50; //表示円の半径
        System.Windows.Media.Brush RightHandLineBrush = System.Windows.Media.Brushes.LightBlue;
        System.Drawing.Color RightHandLineColor = System.Drawing.Color.Blue;

        //ページ遷移用
        NavigationService _navigation;

        //kinect接続関係
        KinectSensor kinect;
        MultiSourceFrameReader multiReader;
        FrameDescription colorFrameDesc;

        //画面表示用
        WriteableBitmap colorImage;
        byte[] colorBuffer;
        int colorStride;
        Int32Rect colorRect;
        ColorImageFormat colorFormat = ColorImageFormat.Bgra;

        //骨格追跡用
        Body[] bodies;
        JointType[] jointType = new[] { JointType.SpineShoulder,
                                        JointType.HandRight,
                                        JointType.ElbowRight
                                        //JointType.HandLeft
                                        };

        //骨格位置の記録のための変数
        List<DataArray> SpineShoulder = new List<DataArray>();
        List<DataArray> HandRight = new List<DataArray>();
        List<DataArray> ElbowRight = new List<DataArray>();
        //List<DataArray> HandLeft = new List<DataArray>();

        //軌跡描画用
        List<ColorSpacePoint> HandRightPoint = new List<ColorSpacePoint>();
        //List<ColorSpacePoint> HandLeftPoint = new List<ColorSpacePoint>();

        //座標保存用
        List<DataArray> R_MasterData3D = new List<DataArray>();
        //List<DataArray> L_MasterData3D = new List<DataArray>();

        //その他のグローバル変数
        int frame = 0;   //フレーム数
        int linecount = 1;
        bool on = false; //録画状態
        bool StartCheck = false;
        bool PlayNow = false;
        DataArray resultArray;
        //bool LeftHandCheck = false;

        //DataArray型構造体
        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        //ページロードしたら行う
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            _navigation = NavigationService; //画面遷移用変数初期化

            string filename = @"image\" + HiddenWordname.Text + "_1.avi";
            Uri uri = new Uri(filename, UriKind.RelativeOrAbsolute);
            GoodModel.Source = uri;

            string resultfilename = @"result\" + HiddenWordname.Text + ".txt";
            using (StreamReader resultsr = new StreamReader(resultfilename, Encoding.Default)) {
                while (resultsr.Peek() >= 0) {
                    resultsr.ReadLine();
                    linecount++;
                }
            }
            Wordname.Text = linecount.ToString();

            //kinectに接続できるかエラーチェック
            try {
                //初期設定
                kinect = KinectSensor.GetDefault();
                kinect.Open();
                colorFrameDesc = kinect.ColorFrameSource.CreateFrameDescription(colorFormat);
                bodies = new Body[kinect.BodyFrameSource.BodyCount];

                //フレームリーダーを開く(データの読み込み準備)
                multiReader = kinect.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Color |
                    FrameSourceTypes.Body);
                multiReader.MultiSourceFrameArrived += MultiReader_MultiSourceFrameArrived;

                //画像枠作成
                colorImage = new WriteableBitmap(
                                    colorFrameDesc.Width, colorFrameDesc.Height,
                                    96, 96, PixelFormats.Bgra32, null); //外枠
                colorBuffer = new byte[colorFrameDesc.LengthInPixels * colorFrameDesc.BytesPerPixel];　//[1920*1080*4]
                colorRect = new Int32Rect(0, 0, colorFrameDesc.Width, colorFrameDesc.Height);
                colorStride = colorFrameDesc.Width * (int)colorFrameDesc.BytesPerPixel; //1行の文字数(1920*4)   
                ImageColor.Source = colorImage;  //表示する
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        //複数のリーダーを同時に開く
        void MultiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            var multiFrame = e.FrameReference.AcquireFrame();
            if (multiFrame == null) {
                return;
            }

            UpdateColorFrame(multiFrame);
            UpdateBodyFrame(multiFrame);
        }

        //カラーフレームを取得
        private void UpdateColorFrame(MultiSourceFrame e) {
            using (var colorFrame = e.ColorFrameReference.AcquireFrame()) {
                if (colorFrame == null) {
                    return;
                }
                colorFrame.CopyConvertedFrameDataToArray(colorBuffer, colorFormat);  //colorデータを取得  
            }
        }

        //ボディフレームを取得
        private void UpdateBodyFrame(MultiSourceFrame e) {
            using (var bodyFrame = e.BodyFrameReference.AcquireFrame()) {
                if (bodyFrame == null) {
                    return;
                }
                bodyFrame.GetAndRefreshBodyData(bodies);  //bodyデータを取得
                DrawFrame();
            }
        }

        private void DrawFrame() {
            colorImage.WritePixels(colorRect, colorBuffer, colorStride, 0);
            DrawBodyFrame();
            if (on) {
                if (frame % NOTEPC == 0) {
                    WriteableBitmap resizeImage;
                    resizeImage = colorImage.Resize(640, 360, WriteableBitmapExtensions.Interpolation.Bilinear); //リサイズ
                    SaveImage(resizeImage, @"image\image" + frame / NOTEPC + ".bmp");
                }
                frame++;
            }
        }

        //WriteableBitmapをbmpファイルとして保存
        private void SaveImage(WriteableBitmap bitmap, string fileName) {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }
        }

        private void DrawBodyFrame() {
            CanvasBody.Children.Clear();
            //bodies[0-5]の中からisTrackedのものだけを対象とする
            foreach (var body in bodies.Where(b => b.IsTracked)) {
                //IReadOnlyDictionary<JointType,Joint> joint = body.Joints;
                foreach (var joint in body.Joints) {
                    for (int i = 0; i < jointType.Length; i++) {
                        if (joint.Value.JointType == jointType[i]) {
                            if (joint.Value.TrackingState == TrackingState.Tracked) {  //位置が追跡状態       
                                DrawEllipse(joint.Value, 15, RightHandLineBrush);
                            } else if (joint.Value.TrackingState == TrackingState.Inferred) {  //位置が推測状態
                                DrawEllipse(joint.Value, 15, System.Windows.Media.Brushes.Yellow);
                            }
                        }
                    }
                }
            }
        }

        private void DrawEllipse(Joint joint, int R, System.Windows.Media.Brush brush) {
            var ellipse = new Ellipse() {
                Width = R,
                Height = R,
                Fill = brush,
            };

            //カメラ座標系をColor座標系に変換する
            var point = kinect.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
            if (point.X < 0 || point.Y < 0) {
                return;
            }

            //Color座標系で骨格位置を描画する
            Canvas.SetLeft(ellipse, point.X / 3 - (R / 2));
            Canvas.SetTop(ellipse, point.Y / 3 - (R / 2));
            CanvasBody.Children.Add(ellipse);

            //位置の記録
            if (on) {
                DataArray tempData;
                tempData.X = joint.Position.X * 100;
                tempData.Y = joint.Position.Y * 100;
                tempData.Z = joint.Position.Z * 100;
                if (joint.JointType == jointType[0]) { //肩中央
                    SpineShoulder.Add(tempData);
                } else if (joint.JointType == jointType[1]) { //右手
                    HandRight.Add(tempData);
                    HandRightPoint.Add(point);
                } else if (joint.JointType == jointType[2]) { //右手
                    ElbowRight.Add(tempData);
                }
            } 
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e) {
            if (StartCheck == false) {
                SleepAsync();
            } else {
                StartCheck = false;
                return;
            }
        }

        private void DrawStartEllipse(DataArray Position) {
            ConfirmCanvas.Children.Clear();
            var ellipse = new Ellipse() {
                Width = RADIUS * 2,
                Height = RADIUS * 2,
                StrokeThickness = 5,
                Stroke = RightHandLineBrush,
            };
            Canvas.SetLeft(ellipse, Position.X - RADIUS);
            Canvas.SetTop(ellipse, Position.Y - RADIUS);
            ConfirmCanvas.Children.Add(ellipse);
        }

        //ボタン入力後の処理
        private async void SleepAsync() {
            float Distance;
            //float RecDistance;
            string B_filename_hand = @"model\BeginerModel_R.txt";
            string B_filename_elbow = @"model\BeginerModel_R_elbow.txt";
            Page2 p2 = new Page2();
            StreamWriter sw;
            int n = 0;
            List<DataArray> Beginer = new List<DataArray>(); //初心者

            //録画開始
            on = true; StartCheck = true;
            CountDown1.Text = "Start";
            while(StartCheck == true) {
                await Task.Delay(1);
            }
            on = false;
            CountDown1.Text = "Stop";
            await Task.Delay(1);

            //骨格位置データ化→DP照合      
            FileRead(@"model\" + HiddenWordname.Text + "_R.txt");
            if (BeginerDataSet(Beginer, true) == true) {
                List<DataArray> BeginerData = new List<DataArray>();
                BeginerData.Add(ClearDataArray());
                for(int i = 0; i < HandRight.Count;i++) {
                    BeginerData.Add(HandRight[i]);
                }
                Distance = DPmatching3(R_MasterData3D, BeginerData);
                n = R_MasterData3D.Count + BeginerData.Count;
                resultArray.X = resultArray.X / n;
                resultArray.Y = resultArray.Y / n;
                resultArray.Z = resultArray.Z / n;
                //RecDistance = (float)1000.0 / Distance;  
                //p2.myName.Text += "　右手：" + matchDistance.ToString("F2") + Environment.NewLine;
                using (sw = new StreamWriter(B_filename_hand, false, Encoding.Default)) {
                    for(int i = 0; i < Beginer.Count; i++) {
                        sw.WriteLine(Beginer[i].X + "," + Beginer[i].Y + "," + Beginer[i].Z + ",");
                    }
                }
                using (sw = new StreamWriter(B_filename_elbow, false, Encoding.Default)) {
                    for (int i = 0; i < ElbowRight.Count; i++) {
                        DataArray tmpData = new DataArray() {
                            X = ElbowRight[i].X - SpineShoulder[i].X,
                            Y = ElbowRight[i].Y - SpineShoulder[i].Y,
                            Z = ElbowRight[i].Z - SpineShoulder[i].Z,
                        };
                        sw.WriteLine(tmpData.X + "," + tmpData.Y + "," + tmpData.Z + ",");
                    }
                }
            } else {
                Distance = -1;
                //RecDistance = -1;
                resultArray = MaxValueDataArray();
            }

            string resultfilename = @"result\" + HiddenWordname.Text + ".txt";
            
            using (StreamWriter resultsw = new StreamWriter(resultfilename, true, Encoding.Default)) {
                resultsw.WriteLine(linecount + " "  + resultArray.X + " " + resultArray.Y + " " + resultArray.Z);
            }

            AviChanger();
            //if (Distance > -1 && resultArray.X < DISTANCE && resultArray.Y < DISTANCE && resultArray.Z < DISTANCE) {
            if(linecount >= 5) { 
                p2.Back.Text = "next";
            }else {
                p2.Back.Text = "back";
            }

            if (Distance != -1) {
                resultArray.X = 100 / resultArray.X;
                if (resultArray.X > 100) {
                    resultArray.X = 100;
                }
                resultArray.Y = 100 / resultArray.Y;
                if (resultArray.Y > 100) {
                    resultArray.Y = 100;
                }
                resultArray.Z = 100 / resultArray.Z;
                if (resultArray.Z > 100) {
                    resultArray.Z = 100;
                }
                p2.myName.Text += "スコア(左右)：" + resultArray.X.ToString("F1") + Environment.NewLine + 
                                  "スコア(上下)：" + resultArray.Y.ToString("F1") + Environment.NewLine + 
                                  "スコア(前後)：" + resultArray.Z.ToString("F1") + Environment.NewLine;
            } else {
                p2.myName.Text += "スコア：エラー" + Environment.NewLine;
            }

            p2.Wordname.Text = Wordname.Text;
            p2.HiddenWordname.Text = HiddenWordname.Text;
            if (multiReader != null) {
                multiReader.Dispose();
                multiReader = null;
            }
            if (kinect != null) {
                kinect.Close();
                kinect = null;
            }
            _navigation.Navigate(p2);
        }

        //ファイルから値を取り出す
        private void FileRead(string fileAdress) {
            const int ARRAYSIZE = 3;
            StreamReader sr;
            DataArray tmpData = new DataArray();
            float[] result = new float[ARRAYSIZE];

            R_MasterData3D.Clear();
            R_MasterData3D.Add(ClearDataArray());
            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            } catch {
                return;
            }

            while(sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string strBuffer = sr.ReadLine();
                String[] stResult = new String[ARRAYSIZE];
                for(int i = 0; i < ARRAYSIZE; i++) {
                    StringLength = strBuffer.IndexOf(",", StringLength); ;
                    stResult[i] = strBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    } catch(FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return;
                    } catch(OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }

                tmpData.X = result[0];
                tmpData.Y = result[1];
                tmpData.Z = result[2];
                R_MasterData3D.Add(tmpData);
            }
            sr.Close();
        }

        //Kinectで取ったデータの処理
        private bool BeginerDataSet(List<DataArray> Beginer, bool Right) {
            DataArray tempData;
            List<DataArray> tempList = new List<DataArray>();
            int i = 0;      

            //エラー処理
            if (SpineShoulder.Count <= 0) {
                return false;
            }

            //相対座標化
            int n = Math.Min(HandRight.Count, SpineShoulder.Count);
            for (i = 0; i < n; i++) {
                tempData.X = HandRight[i].X - SpineShoulder[i].X;
                tempData.Y = HandRight[i].Y - SpineShoulder[i].Y;
                tempData.Z = HandRight[i].Z - SpineShoulder[i].Z;
                tempList.Add(tempData);
            }

            //Beginer配列に代入
            Beginer.Add(ClearDataArray());  //(0,0,0)を加える
            for (i = 0; i < tempList.Count; i++) {
                Beginer.Add(tempList[i]);
            }
            return true;
        }

        //複数のbmpファイルをつなげてavi形式で動画化
        private void AviChanger() {
            int avicount = 0;
            string bmpfilename = @"image\image" + avicount + ".bmp";
            string avifilename = @"image\BeginerModel.avi";
            AVIWriter aviwriter = new AVIWriter();            
            
            System.Drawing.Pen pb = new System.Drawing.Pen(RightHandLineColor, 8);

            if (File.Exists(avifilename)) {
                File.Delete(avifilename);
            }
            if(HandRightPoint.Count > 1) {
                aviwriter.Open(avifilename, 640, 360);
                while (File.Exists(bmpfilename)) {
                    Bitmap bmp = new Bitmap(bmpfilename);
                    Graphics g = Graphics.FromImage(bmp);
                    for (int i = 0; i < avicount * NOTEPC - 1; i++) {
                        if (i < HandRightPoint.Count - 1) {
                            g.DrawLine(pb, HandRightPoint[i].X / 3, HandRightPoint[i].Y / 3, HandRightPoint[i + 1].X / 3, HandRightPoint[i + 1].Y / 3);
                        }
                    }
                    g.Dispose();
                    aviwriter.AddFrame(bmp);
                    bmp.Dispose();
                    avicount++;
                    bmpfilename = @"image\image" + avicount + ".bmp";
                }
            }

            int count = 0;
            bmpfilename = @"image\image" + count + ".bmp";
            while (File.Exists(bmpfilename)) {
                File.Delete(bmpfilename);
                count++;
                bmpfilename = @"image\image" + count + ".bmp";
            }
            aviwriter.Close();
        }

        //DP照合3
        private float DPmatching3(List<DataArray> Master, List<DataArray> Beginer) {
            int M_FrameNumber = Master.Count; //お手本のフレーム長
            int B_FrameNumber = Beginer.Count; //初心者手話のフレーム長
            int i, j;
            float distance = -1;

            DataArray[,] Cost = new DataArray[M_FrameNumber, B_FrameNumber]; //コスト

            Cost[0, 0] = ClearDataArray();

            //→方向に初期値設定
            for (i = 1; i < M_FrameNumber; i++) {
                Cost[i, 0] = MaxValueDataArray();
            }
            for (i = 1; i < M_FrameNumber; i++) {
                Cost[i, 1] = Calc_Add(Cost[i - 1, 0], Calc_Scalor(Calc_Penalty3(Master[i], Beginer[1]), 2), ClearDataArray());
            }

            //↓方向に初期値設定
            for (j = 1; j < B_FrameNumber; j++) {
                Cost[0, j] = MaxValueDataArray();
            }
            for (j = 2; j < B_FrameNumber; j++) {
                Cost[1, j] = Calc_Add(Cost[0, j - 1], Calc_Scalor(Calc_Penalty3(Master[1], Beginer[j]), 2), ClearDataArray());
            }

            //コスト計算
            for (i = 2; i < M_FrameNumber; i++) {
                for (j = 2; j < B_FrameNumber; j++) {
                    DataArray dtemp1 = Calc_Add(Cost[i - 1, j - 1], Calc_Scalor(Calc_Penalty3(Master[i], Beginer[j]), 2), ClearDataArray());
                    DataArray dtemp2 = Calc_Add(Cost[i - 1, j], Calc_Penalty3(Master[i], Beginer[j]), ClearDataArray());
                    DataArray dtemp3 = Calc_Add(Cost[i, j - 1], Calc_Penalty3(Master[i], Beginer[j]), ClearDataArray());

                    Cost[i, j] = MinDataArraySet(dtemp1, dtemp2, dtemp3);
                }
            }
            resultArray = Cost[M_FrameNumber - 1, B_FrameNumber - 1];
            distance = Calc_SumDataArray(Cost[M_FrameNumber - 1, B_FrameNumber - 1]) / (M_FrameNumber + B_FrameNumber);
            return distance;
        }

        //DP照合3 ユークリッド距離計算関数
        private static DataArray Calc_Penalty3(DataArray A, DataArray B) {
            DataArray dataArray = new DataArray {
                X = Math.Abs(A.X - B.X),
                Y = Math.Abs(A.Y - B.Y),
                Z = Math.Abs(A.Z - B.Z)
            };
            return dataArray;
        }

        //DP照合3 定数倍計算
        private static DataArray Calc_Scalor(DataArray A, float n) {
            DataArray dataArray = new DataArray {
                X = A.X * n,
                Y = A.Y * n,
                Z = A.Z * n
            };
            return dataArray;
        }

        //DP照合3 2つのDataArrayの加算
        private static DataArray Calc_Add(DataArray A, DataArray B, DataArray C) {
            DataArray dataArray = new DataArray {
                X = A.X + B.X + C.X,
                Y = A.Y + B.Y + C.Y,
                Z = A.Z + B.Z + C.Z
            };
            return dataArray;
        }

        private static DataArray MinDataArraySet(DataArray A, DataArray B, DataArray C) {
            DataArray dataArray = new DataArray {
                X = Min3(A.X, B.X, C.X),
                Y = Min3(A.Y, B.Y, C.Y),
                Z = Min3(A.Z, B.Z, C.Z),
            };
            return dataArray;
        }

        //3つのデータから最小がどれかを探す関数
        private static float Min3(float a, float b, float c) {
            if (a <= b && a <= c) {
                return a;
            } else if (b <= c) {
                return b;
            } else {
                return c;
            }
        }

        private static float Calc_SumDataArray(DataArray A) {
            return A.X + A.Y + A.Z;
        }

        //DataArray型変数の初期化用
        private static DataArray ClearDataArray() {
            DataArray d = new DataArray {
                X = 0,
                Y = 0,
                Z = 0
            };
            return d;
        }

        private static DataArray MaxValueDataArray() {
            DataArray dataArray = new DataArray {
                X = float.MaxValue,
                Y = float.MaxValue,
                Z = float.MaxValue
            };
            return dataArray;
        }

        private DataArray SetDataArray(float x, float y, float z) {
            DataArray dataArray = new DataArray {
                X = x,
                Y = y,
                Z = z
            };
            return dataArray;
        }

        private void GoodModel_MediaEnded(object sender, RoutedEventArgs e) {
            GoodModel.Stop();
            PlayButtonG.Visibility = Visibility.Visible;
            PlayNow = false;
        }

        private void GoodModel_MouseDown(object sender, MouseButtonEventArgs e) {
            Stop();
        }

        private void PlayButtonG_Click(object sender, RoutedEventArgs e) {
            Play();
        }

        private void Page_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if(PlayNow == false) {
                Play();
            } else {
                Stop();
            }
        }

        private void Play() {
            PlayButtonG.Visibility = Visibility.Hidden;
            GoodModel.Play();
            PlayNow = true;
        }
        private void Stop() {
            GoodModel.Pause();
            PlayButtonG.Visibility = Visibility.Visible;
            PlayNow = false;
        }

        private void SlowCheck_Checked(object sender, RoutedEventArgs e) {
            GoodModel.SpeedRatio = GoodModel.SpeedRatio * 0.5;
        }

        private void SlowCheck_Unchecked(object sender, RoutedEventArgs e) {
            GoodModel.SpeedRatio = GoodModel.SpeedRatio * 2;
        }

        private void Page_MouseRightButtonDown(object sender, MouseButtonEventArgs e) {

        }
    }
}