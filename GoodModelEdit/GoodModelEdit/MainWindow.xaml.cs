﻿using AForge.Video.VFW;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace GoodModelEdit {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        KinectSensor kinect;
        MultiSourceFrameReader multiReader;
        FrameDescription colorFrameDesc;

        //表示用
        WriteableBitmap colorImage;
        byte[] colorBuffer;
        int colorStride;
        Int32Rect colorRect;
        ColorImageFormat colorFormat = ColorImageFormat.Bgra; 

        Body[] bodies;
        JointType[] jointType = new[] {JointType.SpineShoulder,                                        
                                       JointType.HandRight,
                                       JointType.HandLeft
                                       };

        StreamWriter tempwrite;
        StreamWriter ResultWrite;
        List<DataArray> SpineShoulder = new List<DataArray>();
        List<DataArray> HandRight = new List<DataArray>();
        List<DataArray> HandLeft = new List<DataArray>();
        List<ColorSpacePoint> HandRightPoint = new List<ColorSpacePoint>();
        List<ColorSpacePoint> HandLeftPoint = new List<ColorSpacePoint>();
        int frame = 0; //フレーム数
        int buttonCount = 0; //ボタンを押した回数
        bool on = false;
        bool onlyRightHandCheck;
        string wordname = ""; //ひとつ前の単語名の保存

        //DataArray構造体
        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            try {
                //kinectと接続                
                kinect = KinectSensor.GetDefault();
                kinect.Open();
                colorFrameDesc = kinect.ColorFrameSource.CreateFrameDescription(colorFormat);

                //bodyを入れる配列を作る
                bodies = new Body[kinect.BodyFrameSource.BodyCount];

                //マルチフレームリーダーを開く(データの読み込み準備)
                multiReader = kinect.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Color |
                    //FrameSourceTypes.Depth |
                    FrameSourceTypes.Body);
                multiReader.MultiSourceFrameArrived += multiReader_MultiSourceFrameArrived;

                colorImage = new WriteableBitmap(
                                    colorFrameDesc.Width, colorFrameDesc.Height,
                                    96, 96, PixelFormats.Bgra32, null); //外枠
                colorBuffer = new byte[colorFrameDesc.LengthInPixels * colorFrameDesc.BytesPerPixel];　//[1920*1080*4]
                colorRect = new Int32Rect(0, 0, colorFrameDesc.Width, colorFrameDesc.Height);
                colorStride = colorFrameDesc.Width * (int)colorFrameDesc.BytesPerPixel; //1行の文字数   
                ImageColor.Source = colorImage;
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        void multiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            var multiFrame = e.FrameReference.AcquireFrame();
            if (multiFrame == null) {
                return;
            }

            UpdateColorFrame(multiFrame);
            UpdateBodyFrame(multiFrame);

            DrawFrame();
        }

        private void UpdateColorFrame(MultiSourceFrame e) {
            //カラーフレームを取得
            using (var colorFrame = e.ColorFrameReference.AcquireFrame()) {
                if (colorFrame == null) {
                    return;
                }

                //colorデータを取得
                colorFrame.CopyConvertedFrameDataToArray(colorBuffer, colorFormat);
            }
        }

        private void UpdateBodyFrame(MultiSourceFrame e) {
            using (var bodyFrame = e.BodyFrameReference.AcquireFrame()) {
                if (bodyFrame == null) {
                    return;
                }

                //bodyデータを取得
                bodyFrame.GetAndRefreshBodyData(bodies);
                if (bodies == null) {
                    return;
                }
            }
        }

        private void DrawFrame() {
            colorImage.WritePixels(colorRect, colorBuffer, colorStride, 0);
            DrawBodyFrame();
            if (on) {
                WriteableBitmap resizeImage;
                resizeImage = colorImage.Resize(640, 360, WriteableBitmapExtensions.Interpolation.Bilinear); //リサイズ
                SaveImage(resizeImage, @"image\test" + frame + ".bmp");
                frame++;
            }
        }

        //bitmapをbmpファイルとして保存
        private void SaveImage(WriteableBitmap bitmap, string fileName) {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }
        }

        private void DrawBodyFrame() {
            CanvasBody.Children.Clear();
            //bodies[0-5]の中からisTrackedのものだけを対象とする
            foreach (Body body in bodies.Where(b => b.IsTracked)) {
                //IReadOnlyDictionary<JointType,Joint> joint = body.Joints;
                foreach (var joint in body.Joints) {
                    for (int i = 0; i < jointType.Length; i++) {
                        if (joint.Value.JointType == jointType[i]) {
                            if (joint.Value.TrackingState == TrackingState.Tracked) {  //追跡状態                               
                                DrawEllipse(joint.Value, 10, System.Windows.Media.Brushes.Blue);
                            } else if (joint.Value.TrackingState == TrackingState.Inferred) {  //推測状態
                                DrawEllipse(joint.Value, 10, System.Windows.Media.Brushes.Yellow);
                            } 
                        }
                    }
                }
            }
        }


        private void DrawEllipse(Joint joint, int R, System.Windows.Media.Brush brush) {
            var ellipse = new Ellipse() {
                Width = R,
                Height = R,
                Fill = brush,
            };

            //カメラ座標系をColor座標系に変換する
            ColorSpacePoint point = kinect.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
            if (point.X < 0 || point.Y < 0) {
                return;
            }

            //Color座標系で円を配置する
            Canvas.SetLeft(ellipse, point.X / 3 - (R / 2));
            Canvas.SetTop(ellipse, point.Y / 3 - (R / 2));
            CanvasBody.Children.Add(ellipse);

            if (on) {
                DataArray tempData;
                tempData.X = joint.Position.X * 100;
                tempData.Y = joint.Position.Y * 100;
                tempData.Z = joint.Position.Z * 100;
                if (joint.JointType == jointType[0]) { //肩中央
                    SpineShoulder.Add(tempData);
                } else if (joint.JointType == jointType[1]) { //右手
                    HandRight.Add(tempData);
                    HandRightPoint.Add(point);
                } else if (joint.JointType == jointType[2]) { //左手
                    HandLeft.Add(tempData);
                    HandLeftPoint.Add(point);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            string filename = @"model\" + Textbox1.Text + "_R.txt";

            if (Textbox1.Text.Length == 0) {
                CarefulText.Text = "単語を入力してください";
                CarefulText.Foreground = System.Windows.Media.Brushes.Red;
                CarefulText.FontWeight = FontWeights.Bold;
                buttonCount = 0;
                wordname = "";
                return;
            }
            if (File.Exists(filename)) {
                CarefulText.Foreground = System.Windows.Media.Brushes.Red;
                CarefulText.FontWeight = FontWeights.Bold;
                if (wordname != Textbox1.Text) {
                    buttonCount = 0;
                }
                switch (buttonCount) {
                    case 0:
                        CarefulText.Text = "同じ名前のファイルが存在します";
                        buttonCount++;
                        wordname = Textbox1.Text;
                        return;
                    case 1:
                        CarefulText.Text = "もう一度押すとファイルを上書きします";
                        buttonCount++;
                        return;
                    default:
                        File.Delete(filename);
                        CarefulText.Text = "ファイルを更新します";
                        buttonCount = 0;
                        break;
                }
            }

            tempwrite = new StreamWriter(@"image\temp1.txt", false, Encoding.Default);
            ResultWrite = new StreamWriter(@"model\" + Textbox1.Text + "_R.txt", false, Encoding.Default);

            Button.Click -= Button_Click;
            sleepAsync();
        }

        //表示と遷移(カウントダウンとカウントアップ)
        private async void sleepAsync() {
            onlyRightHandCheck = (bool)OneHand.IsChecked;
            CountDown1.Text = "4";
            await Task.Delay(1000);
            CountDown1.Text = "3";
            await Task.Delay(1000);
            CountDown1.Text = "2";
            await Task.Delay(1000);
            CountDown1.Text = "1";
            await Task.Delay(1000);
            CountDown1.Text = "Start";
            on = true;

            await Task.Delay(1000);
            //CountDown1.Foreground = System.Windows.Media.Brushes.Red;
            CountDown1.Text = "1";
            await Task.Delay(1000);
            CountDown1.Text = "2";
            await Task.Delay(1000);
            CountDown1.Text = "3";
            await Task.Delay(1000);
            CountDown1.Text = "4";
            await Task.Delay(1000);
            CountDown1.Text = "5";
            await Task.Delay(1000);
            CountDown1.Text = "Stop";
            await Task.Delay(10);
            on = false;           

            bool RightRecordState = RecordDataSet(true);
            bool LeftRecordState = true;
            if (onlyRightHandCheck == false) {
                ResultWrite.Close();
                ResultWrite = new StreamWriter(@"model\" + Textbox1.Text + "_L.txt", false, Encoding.Default);
                LeftRecordState = RecordDataSet(false);
            }

            if (!(RightRecordState && LeftRecordState)) {
                CarefulText.Foreground = System.Windows.Media.Brushes.Red;
                CarefulText.FontWeight = FontWeights.Bold;
                await Task.Delay(2000);
            }

            tempwrite.Close();
            ResultWrite.Close();
            Reset();
        }

        private void Reset() {
            CountDown1.Text = "";
            CarefulText.Text = "単語を入力してください";
            CarefulText.Foreground = System.Windows.Media.Brushes.Black;
            CarefulText.FontWeight = FontWeights.Normal;
            Button.Click += Button_Click;
            HandRight.Clear();
            HandRightPoint.Clear();
            HandLeft.Clear();
            HandLeftPoint.Clear();
            frame = 0;

            int i = 0;
            string bmpfilename = @"image\test" + i + ".bmp";
            while (File.Exists(bmpfilename)) {
                File.Delete(bmpfilename);
                i++;
                bmpfilename = @"image\test" + i + ".bmp";
            }
        }

        private bool RecordDataSet(bool p) {
            DataArray tempData;
            List<DataArray> tempList = new List<DataArray>();            
            int i = 0;
            int Start;
            int Stop;

            //エラー処理
            if (SpineShoulder.Count <= 0) {     
                CarefulText.Text = "骨格追跡がうまくいきませんでした";
                return false;
            }

            for (i = 0; i < HandRight.Count; i++) {
                if (p) {
                    tempData.X = HandRight[i].X - SpineShoulder[i].X;
                    tempData.Y = HandRight[i].Y - SpineShoulder[i].Y;
                    tempData.Z = HandRight[i].Z - SpineShoulder[i].Z;
                } else {
                    tempData.X = HandLeft[i].X - SpineShoulder[i].X;
                    tempData.Y = HandLeft[i].Y - SpineShoulder[i].Y;
                    tempData.Z = HandLeft[i].Z - SpineShoulder[i].Z;
                }
                tempList.Add(tempData);
            }

            //tempファイルに相対座標データを書き込む(セグメンテーション前)
            for(i = 0; i < tempList.Count; i++) {
                tempwrite.WriteLine(tempList[i].X + "," + tempList[i].Y + "," + tempList[i].Z + ",");
            }

            ResultWrite.WriteLine("0,0,0,");
            Start = Segmentation(12, 12, 10, tempList, true);
            if (Start < 0) {
                return false;
            }
            Start = Segmentation(Start, 3, 5, tempList, true);
            Start = Segmentation(Start, 1, 1, tempList, true);

            Stop = Segmentation(tempList.Count - 1, 12, 10, tempList, false);
            if (Stop < 0) {
                return false;
            }
            Stop = Segmentation(Stop, 3, 5, tempList, false);
            Stop = Segmentation(Stop, 1, 1, tempList, false);           

            for (i = Start; i <= Stop; i++) {
                ResultWrite.WriteLine(tempList[i].X + "," + tempList[i].Y + "," + tempList[i].Z + ", , , ," + HandRightPoint[i].X + "," + HandRightPoint[i].Y + ",");
            }

            if (p) {
                aviChanger(Start, Stop);
            }
            return true;
        }

        private void aviChanger(int start, int stop) {
            int count = start;
            string bmpfilename = @"image\test" + count + ".bmp";
            string avifilename = @"image\" + Textbox1.Text + ".avi";            
            AVIWriter aviwriter = new AVIWriter();
            System.Drawing.Pen pb = new System.Drawing.Pen(System.Drawing.Color.Blue, 3);
            System.Drawing.Pen pr = new System.Drawing.Pen(System.Drawing.Color.Red, 3);

            if (File.Exists(avifilename)) {
                File.Delete(avifilename);
            }

            aviwriter.Open(avifilename, 640, 360);
            while (File.Exists(bmpfilename) && count <= stop) {
                Bitmap bmp = new Bitmap(bmpfilename);
                Graphics g = Graphics.FromImage(bmp);
                if (HandRightPoint.Count != 0) {
                    for (int i = start; i < count; i++) {
                        g.DrawLine(pb, HandRightPoint[i].X / 3, HandRightPoint[i].Y / 3, HandRightPoint[i + 1].X / 3, HandRightPoint[i + 1].Y / 3);
                    }
                }
                if (onlyRightHandCheck == false && HandLeftPoint.Count != 0) {
                    for (int i = start; i < count; i++) {
                        g.DrawLine(pr, HandLeftPoint[i].X / 3, HandLeftPoint[i].Y / 3, HandLeftPoint[i + 1].X / 3, HandLeftPoint[i + 1].Y / 3);
                    }
                }
                g.Dispose();
                aviwriter.AddFrame(bmp);
                bmp.Dispose();
                count++;
                bmpfilename = @"image\test" + count + ".bmp";
            }           
            aviwriter.Close();
        }

        private int Segmentation(int Start, int a, float max, List<DataArray> tempList, bool StartCheck) {
            if (StartCheck) {
                while (Start < tempList.Count && (tempList[Start].Y - tempList[Start - a].Y < max)) {
                    Start = Start + a;
                    if (Start > tempList.Count) {
                        CarefulText.Text = "セグメンテーションがうまくいきませんでした";
                        return -1;
                    }
                }
                return Start - a;
            } else {
                while (tempList[Start - a].Y - tempList[Start].Y < max) {
                    Start = Start - a;
                    if (Start > tempList.Count) {
                        CarefulText.Text = "セグメンテーションがうまくいきませんでした";
                        return -1;
                    }
                }
                return Start + a;
            }
        }

        private DataArray clearDataArray() {
            DataArray d;
            d.X = 0; d.Y = 0; d.Z = 0;
            return d;
        }

        private DataArray MinDataArray() {
            DataArray d;
            d.X = float.MinValue; d.Y = float.MinValue; d.Z = float.MinValue;
            return d;
        }

        private void Window_Closed(object sender, EventArgs e) {
            int filenumber = 0;
            string filename = @"image\test0.bmp";
            while (File.Exists(filename)) {
                File.Delete(filename);
                filenumber++;
                filename = @"image\test" + filenumber + ".bmp";
            }
        }     
    }
}  