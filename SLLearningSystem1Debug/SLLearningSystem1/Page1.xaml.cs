﻿using AForge.Video.VFW;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SLLearningSystem1 {
    /// <summary>
    /// Page1.xaml の相互作用ロジック
    /// </summary>
    public partial class Page1 : Page {
        public Page1() {
            InitializeComponent();
        }

        const bool debug = true;

        //ページ遷移用
        NavigationService _navigation;

        //kinect接続関係
        KinectSensor kinect;
        MultiSourceFrameReader multiReader;
        FrameDescription colorFrameDesc;

        //表示用
        WriteableBitmap colorImage;
        byte[] colorBuffer;
        int colorStride;
        Int32Rect colorRect;
        ColorImageFormat colorFormat = ColorImageFormat.Bgra; 

        //骨格追跡用
        Body[] bodies;
        JointType[] jointType = new[] { JointType.SpineShoulder, 
                                        JointType.HandRight, 
                                        JointType.HandLeft 
                                        };

        //録画処理等に必要なグローバル変数
        List<DataArray> SpineShoulder = new List<DataArray>();
        List<DataArray> HandRight = new List<DataArray>();
        List<DataArray> HandLeft = new List<DataArray>();
        List<ColorSpacePoint> HandRightPoint = new List<ColorSpacePoint>();
        List<ColorSpacePoint> HandLeftPoint = new List<ColorSpacePoint>();
        int frame = 0; //フレーム数
        bool on = false;
        
        //DataArray型構造体
        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }
      
        //ページをロードしたらまず
        private void Page_Loaded(object sender, RoutedEventArgs e) {
            _navigation = this.NavigationService;
            _navigation.Navigating += new NavigatingCancelEventHandler(_navigation_Navigating);
            _navigation.Navigated += new NavigatedEventHandler(_navigation_Navigated);

            try {
                //kinectと接続                
                kinect = KinectSensor.GetDefault();
                kinect.Open();
                colorFrameDesc = kinect.ColorFrameSource.CreateFrameDescription(colorFormat);

                //bodyを入れる配列を作る
                bodies = new Body[kinect.BodyFrameSource.BodyCount];

                //フレームリーダーを開く(データの読み込み準備)
                multiReader = kinect.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Color |
                    //FrameSourceTypes.Depth |
                    FrameSourceTypes.Body);
                multiReader.MultiSourceFrameArrived += multiReader_MultiSourceFrameArrived;

                //画像枠作成
                colorImage = new WriteableBitmap(
                                    colorFrameDesc.Width, colorFrameDesc.Height,
                                    96, 96, PixelFormats.Bgra32, null); //外枠
                colorBuffer = new byte[colorFrameDesc.LengthInPixels * colorFrameDesc.BytesPerPixel];　//[1920*1080*4]
                colorRect = new Int32Rect(0, 0, colorFrameDesc.Width, colorFrameDesc.Height);
                colorStride = colorFrameDesc.Width * (int)colorFrameDesc.BytesPerPixel; //1行の文字数   
                ImageColor.Source = colorImage;
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        void multiReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e) {
            var multiFrame = e.FrameReference.AcquireFrame();
            if (multiFrame == null) {
                return;
            }

            UpdateColorFrame(multiFrame);
            UpdateBodyFrame(multiFrame);

            DrawFrame();
        }

        private void UpdateColorFrame(MultiSourceFrame e) {
            //カラーフレームを取得
            using (var colorFrame = e.ColorFrameReference.AcquireFrame()) {
                if (colorFrame == null) {
                    return;
                }

                //colorデータを取得   
                colorFrame.CopyConvertedFrameDataToArray(colorBuffer, colorFormat);
            }
        }

        private void UpdateBodyFrame(MultiSourceFrame e) {
            using (var bodyFrame = e.BodyFrameReference.AcquireFrame()) {
                if (bodyFrame == null) {
                    return;
                }

                //bodyデータを取得
                bodyFrame.GetAndRefreshBodyData(bodies);
                if (bodies == null) {
                    return;
                }
            }
        }

        private void DrawFrame() {
            colorImage.WritePixels(colorRect, colorBuffer, colorStride, 0);
            DrawBodyFrame();
            if (on) {
                WriteableBitmap resizeImage;
                resizeImage = colorImage.Resize(640, 360, WriteableBitmapExtensions.Interpolation.Bilinear); //リサイズ
                SaveImage(resizeImage, @"image\image" + frame + ".bmp");
                frame++;
            }
        }

        //WriteableBitmapをbmpファイルとして保存
        private void SaveImage(WriteableBitmap bitmap, string fileName) {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(stream);
            }           
        }

        private void DrawBodyFrame() {
            CanvasBody.Children.Clear();
            //bodies[0-5]の中からisTrackedのものだけを対象とする
            foreach (var body in bodies.Where(b => b.IsTracked)) {
                //IReadOnlyDictionary<JointType,Joint> joint = body.Joints;
                foreach (var joint in body.Joints) {
                    for (int i = 0; i < jointType.Length; i++) {
                        if (joint.Value.JointType == jointType[i]) {
                            if (joint.Value.TrackingState == TrackingState.Tracked) {  //手の位置が追跡状態                               
                                DrawEllipse(joint.Value, 10, System.Windows.Media.Brushes.Blue);
                            } else if (joint.Value.TrackingState == TrackingState.Inferred) {  //手の位置が推測状態
                                DrawEllipse(joint.Value, 10, System.Windows.Media.Brushes.Yellow);
                            } 
                        }
                    }
                }
            }
        }

        private void DrawEllipse(Joint joint, int R, System.Windows.Media.Brush brush) {
            var ellipse = new Ellipse() {
                Width = R,
                Height = R,
                Fill = brush,
            };

            //カメラ座標系をColor座標系に変換する
            ColorSpacePoint point = kinect.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
            if (point.X < 0 || point.Y < 0) {
                return;
            }

            //Color座標系で円を配置する
            //colorImage.FillEllipseCentered((int)point.X, (int)point.Y, R, R, color);
            Canvas.SetLeft(ellipse, point.X / 3 - (R / 2));
            Canvas.SetTop(ellipse, point.Y / 3 - (R / 2));
            CanvasBody.Children.Add(ellipse);

            if (on) {
                DataArray tempData;
                tempData.X = joint.Position.X * 100;
                tempData.Y = joint.Position.Y * 100;
                tempData.Z = joint.Position.Z * 100;
                if (joint.JointType == jointType[0]) { //肩中央
                    SpineShoulder.Add(tempData);
                } else if (joint.JointType == jointType[1]) { //右手
                    HandRight.Add(tempData);
                    HandRightPoint.Add(point);
                } else if (joint.JointType == jointType[2]) { //左手
                    HandLeft.Add(tempData);
                    HandLeftPoint.Add(point);
                }
            }                
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            string filename = @"model\" + Textbox1.Text + "_R.txt";
            if (Textbox1.Text.Length == 0) {
                CarefulText.Text = "単語を入力してください";
                CarefulText.Foreground = System.Windows.Media.Brushes.Red;
                CarefulText.FontWeight = FontWeights.Bold;
                return;
            }
            if (!File.Exists(filename)) {
                CarefulText.Text = "お手本がありません";
                CarefulText.Foreground = System.Windows.Media.Brushes.Red;
                CarefulText.FontWeight = FontWeights.Bold;
                return;
            }
            Button.Click -= Button_Click;

            sleepAsync();       
        }

        //表示と遷移
        private void sleepAsync() {
            float distance;
            bool BothHandsCheck;
            char onestring = Textbox1.Text[0]; 
            string b_filename = @"model\valley_R.txt";
            StreamWriter sw;
            Page2 p2 = new Page2();

            p2.hidden.Text = Textbox1.Text;
            p2.myName.Text = Textbox1.Text + " : " + Environment.NewLine;
            List<DataArray> Master = new List<DataArray>(); //お手本   
            List<DataArray> Beginer = new List<DataArray>(); //初心者
            string M_filenameR = @"model\" + Textbox1.Text + "_R.txt";
            string M_filenameL = @"model\" + Textbox1.Text + "_L.txt";
            BothHandsCheck = File.Exists(M_filenameL);          

            if (fileRead(Beginer, b_filename) && fileRead(Master, M_filenameR)) {
                distance = DPmatching(Master, Beginer);
                p2.myName.Text += "　右手：" + distance.ToString("F2") + Environment.NewLine;

                if (BothHandsCheck == true) {
                    Master.Clear();
                    Beginer.Clear();
                    b_filename = @"model\valley_L.txt";
                    if (fileRead(Beginer, b_filename) && fileRead(Master, M_filenameL)) {
                        float tempdistance = DPmatching(Master, Beginer);
                        p2.myName.Text += "　左手：" + tempdistance.ToString("F2") + Environment.NewLine;
                        if (tempdistance > distance) {
                            distance = tempdistance;
                        }       
                    } else {
                        distance = -1;
                    }
                }
            } else {
                distance = -1;
            }

            if (distance != -1) {
                p2.myName.Text += "　総合：" + distance.ToString("F2") + Environment.NewLine;
            } else {
                p2.myName.Text += "　総合：エラー" + Environment.NewLine;
            }

            _navigation.Navigate(p2);
        }
      
        private static bool fileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 3;
            StreamReader sr;
            DataArray tmpData = new DataArray();
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            } catch {
                return false;
            }

            while (sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for (i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    }
                    catch (FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    catch (OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }

                tmpData.X = result[0];
                tmpData.Y = result[1];
                tmpData.Z = result[2];
                list.Add(tmpData);
            }
            sr.Close();
            return true;
        }

        private bool BeginerDataSet(List<DataArray> Beginer, bool p) {
            DataArray tempData;
            List<DataArray> tempList = new List<DataArray>();
            int i = 0;
            int Start, Stop;           

            //エラー処理
            if (SpineShoulder.Count <= 0) {
                aviChanger(0, 150);
                return false;
            }

            for (i = 0; i < HandRight.Count; i++) {
                if (p) {
                    tempData.X = HandRight[i].X - SpineShoulder[i].X;
                    tempData.Y = HandRight[i].Y - SpineShoulder[i].Y;
                    tempData.Z = HandRight[i].Z - SpineShoulder[i].Z;
                } else {
                    tempData.X = HandLeft[i].X - SpineShoulder[i].X;
                    tempData.Y = HandLeft[i].Y - SpineShoulder[i].Y;
                    tempData.Z = HandLeft[i].Z - SpineShoulder[i].Z;
                }
                tempList.Add(tempData);
            }

            Beginer.Add(clearDataArray());
            Start = Segmentation(12, 12, 10, tempList, true);
            if (Start < 0) {
                return false;
            }
            Start = Segmentation(Start, 3, 5, tempList, true);
            Start = Segmentation(Start, 1, 1, tempList, true);

            Stop = Segmentation(tempList.Count - 1, 12, 10, tempList, false);
            if (Stop < 0) {
                return false;
            }
            Stop = Segmentation(Stop, 3, 5, tempList, false);
            Stop = Segmentation(Stop, 1, 1, tempList, false);

            for (i = Start; i < Stop; i++) {
                Beginer.Add(tempList[i]);
                
            }

            if (p) {
                aviChanger(Start, Stop);
            }
            return true;
        }

        //複数のbmpファイルをつなげてavi形式で動画化
        private void aviChanger(int start, int stop) {
            int count = start;
            string bmpfilename = @"image\image" + count + ".bmp";
            string avifilename = @"image\BeginerModel.avi";
            AVIWriter aviwriter = new AVIWriter();
            System.Drawing.Pen pb = new System.Drawing.Pen(System.Drawing.Color.Blue, 3);
            System.Drawing.Pen pr = new System.Drawing.Pen(System.Drawing.Color.Red, 3);

            if (File.Exists(avifilename)) {
                File.Delete(avifilename);
            }

            aviwriter.Open(avifilename, 640, 360);
            while (File.Exists(bmpfilename) && count <= stop) {
                Bitmap bmp = new Bitmap(bmpfilename);
                Graphics g = Graphics.FromImage(bmp);
                if (HandRightPoint.Count != 0) {
                    for (int i = start; i < count; i++) {
                        g.DrawLine(pb, HandRightPoint[i].X / 3, HandRightPoint[i].Y / 3, HandRightPoint[i + 1].X / 3, HandRightPoint[i + 1].Y / 3);
                    }
                }
                if (HandLeftPoint.Count != 0) {
                    for (int i = start; i < count; i++) {
                        g.DrawLine(pr, HandLeftPoint[i].X / 3, HandLeftPoint[i].Y / 3, HandLeftPoint[i + 1].X / 3, HandLeftPoint[i + 1].Y / 3);
                    }
                }
                g.Dispose();
                aviwriter.AddFrame(bmp);
                bmp.Dispose();
                count++;
                bmpfilename = @"image\image" + count + ".bmp";
            }

            count = 0;
            bmpfilename = @"image\image" + count + ".bmp";
            while (File.Exists(bmpfilename)) {
                File.Delete(bmpfilename);
                count++;
                bmpfilename = @"image\image" + count + ".bmp";
            }
            aviwriter.Close();
        }

        private int Segmentation(int Start, int a, float max, List<DataArray> tempList, bool StartCheck) {
            if (StartCheck) {
                while (Start < tempList.Count && (tempList[Start].Y - tempList[Start - a].Y < max)) {
                    Start = Start + a;
                    if (Start > tempList.Count) {
                        return -1;
                    }
                }
                return Start - a;
            } else {
                while (tempList[Start - a].Y - tempList[Start].Y < max) {
                    Start = Start - a;
                    if (Start > tempList.Count) {
                        return -1;
                    }
                }
                return Start + a;
            }
        }

        //DP照合
        private float DPmatching(List<DataArray> Master, List<DataArray> Beginer) {
            const int DIAGONAL = 0; //斜め
            const int RIGHTUNDER = 1; //→↓↓
            const int UNDERRIGHT = 2; //↓→→                                      
            int M_FrameNumber; //お手本のフレーム長
            int B_FrameNumber; //初心者手話のフレーム長
            int i, j;
            float distance = -1;

            M_FrameNumber = Master.Count;
            B_FrameNumber = Beginer.Count;

            float[,] Cost = new float[M_FrameNumber, B_FrameNumber];
            int[,] From = new int[M_FrameNumber, B_FrameNumber];
            int[,] c = new int[M_FrameNumber, B_FrameNumber];

            c[0, 0] = 0;
            Cost[0, 0] = 0;
            From[0, 0] = -1;

            //→方向に初期値設定
            for (i = 1; i < M_FrameNumber; i++) {
                c[i, 0] = 0;
                Cost[i, 0] = float.MaxValue;
                From[i, 0] = -1;
            }
            for (i = 1; i < M_FrameNumber; i++) {
                c[i, 1] = 2;
                Cost[i, 1] = Cost[i - 1, 0] + Calc_Penalty(Master[i], Beginer[1]) * 2;
                From[i, 1] = 0;
            }

            //↓方向に初期値設定
            for (j = 1; j < B_FrameNumber; j++) {
                c[0, j] = 0;
                Cost[0, j] = float.MaxValue;
                From[0, j] = -1;
            }
            for (j = 2; j < B_FrameNumber; j++) {
                c[1, j] = 2;
                Cost[1, j] = Cost[0, j - 1] + Calc_Penalty(Master[1], Beginer[j]) * 2;
                From[1, j] = 0;
            }

            //コスト計算
            for (i = 2; i < M_FrameNumber; i++) {
                for (j = 2; j < B_FrameNumber; j++) {
                    float dtemp1 = Cost[i - 1, j - 1] + Calc_Penalty(Master[i], Beginer[j]) * 2;
                    float dtemp2 = Cost[i - 2, j - 1] + Calc_Penalty(Master[i - 1], Beginer[j]) * 2 + Calc_Penalty(Master[i], Beginer[j]);
                    float dtemp3 = Cost[i - 1, j - 2] + Calc_Penalty(Master[i], Beginer[j - 1]) * 2 + Calc_Penalty(Master[i], Beginer[j]);

                    int min = Min(dtemp1, dtemp2, dtemp3);
                    if (min == DIAGONAL) {
                        c[i, j] = c[i - 1, j - 1] + 2;
                        Cost[i, j] = dtemp1;
                        From[i, j] = DIAGONAL;
                    } else if (min == RIGHTUNDER) {
                        c[i, j] = c[i - 2, j - 1] + 3;
                        Cost[i, j] = dtemp2;
                        From[i, j] = RIGHTUNDER;
                    } else {
                        c[i, j] = c[i - 1, j - 2] + 3;
                        Cost[i, j] = dtemp3;
                        From[i, j] = UNDERRIGHT;
                    }
                }
            }
            distance = Cost[M_FrameNumber - 1, B_FrameNumber - 1] / c[M_FrameNumber - 1, B_FrameNumber - 1];
            return distance;
        }

        //ユークリッド距離計算関数
        private static float Calc_Penalty(DataArray A, DataArray B) {
            double X_dif, Y_dif, Z_dif;
            X_dif = Math.Pow((A.X - B.X), 2);
            Y_dif = Math.Pow((A.Y - B.Y), 2);
            Z_dif = Math.Pow((A.Z - B.Z), 2);
            return (float)Math.Sqrt(X_dif + Y_dif + Z_dif);
        }

        //3つのデータから最小がどれかを探す関数
        private static int Min(float a, float b, float c) {
            if (a <= b && a <= c) {
                return 0;
            } else if (b <= c) {
                return 1;
            } else {
                return 2;
            }
        }

        private DataArray clearDataArray() {
            DataArray d;
            d.X = 0; d.Y = 0; d.Z = 0;
            return d;
        }

        private DataArray MinDataArray() {
            DataArray d;
            d.X = float.MinValue; d.Y = float.MinValue; d.Z = float.MinValue;
            return d;
        }

        private void _navigation_Navigated(object sender, NavigationEventArgs e) {
            _navigation.Navigated -= this._navigation_Navigated;
            _navigation.Navigating -= this._navigation_Navigating;
            _navigation = null;
        }

        private void _navigation_Navigating(object sender, NavigatingCancelEventArgs e) {
            if (e.NavigationMode == NavigationMode.New && Textbox1.Text.Length == 0) {
                e.Cancel = true;
                CarefulText.Foreground = System.Windows.Media.Brushes.Red;
                CarefulText.FontWeight = FontWeights.Bold;
            }
        }

        private void Page_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                Button_Click(sender, e);
            }
        }
    }
}