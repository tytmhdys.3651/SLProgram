﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SLLearningSystem1 {
    /// <summary>
    /// Zaxis.xaml の相互作用ロジック
    /// </summary>
    public partial class Zaxis : Window {
        public Zaxis() {
            InitializeComponent();
        }
        DispatcherTimer dispatcherTimer;
        List<DataArray> Beginer;
        int BeginerDatasetNumber;
        List<DataArray> Master;
        int MasterDatasetNumber;
        int timecheck;

        struct DataArray {
            public float X;
            public float Y;
            public float Z;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            char onestring = wordname.Text[0];
            string M_filenameR = @"model\" + wordname.Text + "_R.txt";
            string M_filenameL = @"model\" + wordname.Text + "_L.txt";
            string B_filenameR = @"model\valley_R.txt";
            string B_filenameL = @"model\valley_L.txt";

            Beginer = new List<DataArray>();
            Master = new List<DataArray>();
            fileRead(Master, M_filenameR);
            fileRead(Beginer, B_filenameR);
            MasterDatasetNumber = Master.Count;
            BeginerDatasetNumber = Beginer.Count;
            fileRead(Master, M_filenameL);
            fileRead(Beginer, B_filenameL);

            visSide();
        }

        private static bool fileRead(List<DataArray> list, string fileAdress) {
            const int ARRAYSIZE = 3;
            StreamReader sr;
            DataArray tmpData;
            String[] stResult = new String[ARRAYSIZE];
            float[] result = new float[ARRAYSIZE];
            int i, j;

            try {
                sr = new StreamReader(fileAdress, Encoding.Default);
            }
            catch {
                return false;
            }

            while (sr.Peek() >= 0) {
                int preStringLength = 0;
                int StringLength = 0;
                string stBuffer = sr.ReadLine();
                for (i = 0; i < ARRAYSIZE; i++) {
                    j = stBuffer.IndexOf(",", StringLength);
                    StringLength = j;
                    stResult[i] = stBuffer.Substring(preStringLength, StringLength - preStringLength);
                    try {
                        result[i] = float.Parse(stResult[i]);
                    }
                    catch (FormatException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    catch (OverflowException ex) {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                    StringLength++;
                    preStringLength = StringLength;
                }
                tmpData.X = result[0];
                tmpData.Y = result[1];
                tmpData.Z = result[2];
                list.Add(tmpData);
            }
            sr.Close();
            return true;
        }

        private void playButton_Click(object sender, RoutedEventArgs e) {
            ButtonClickProcess();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Start();
        }

        private void slowButton_Click(object sender, RoutedEventArgs e) {
            ButtonClickProcess();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Start();
        }

        private void ButtonClickProcess() {
            playButton.Click -= playButton_Click;
            slowButton.Click -= slowButton_Click;
            radioButtonSet.IsEnabled = false;
            timecheck = 1;
            CanvasBody.Children.Clear();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e) {
            int max = Math.Max(BeginerDatasetNumber, MasterDatasetNumber);
            CanvasBody.Children.Clear();
            if (timecheck < max) {
                if (XZ.IsChecked == true) {
                    visHead();
                    drawEllipse(timecheck, true);
                } else if (YZ.IsChecked == true) {
                    visSide();
                    drawEllipse(timecheck, false);
                }
            } else {
                dispatcherTimer.Stop();
                radioButtonSet.IsEnabled = true;
                playButton.Click += playButton_Click;
                slowButton.Click += slowButton_Click;
            }
            timecheck++;
        }

        private void visHead() {
            headLine.Visibility = Visibility.Visible;
            headEll.Visibility = Visibility.Visible;
            headPol.Visibility = Visibility.Visible;
            sideEll.Visibility = Visibility.Hidden;
            sidePol.Visibility = Visibility.Hidden;
            sideChair1.Visibility = Visibility.Hidden;
            sideChair2.Visibility = Visibility.Hidden;
        }
        private void visSide() {
            headLine.Visibility = Visibility.Hidden;
            headEll.Visibility = Visibility.Hidden;
            headPol.Visibility = Visibility.Hidden;
            sideEll.Visibility = Visibility.Visible;
            sidePol.Visibility = Visibility.Visible;
            sideChair1.Visibility = Visibility.Visible;
            sideChair2.Visibility = Visibility.Visible;
        }

        private void drawEllipse(int i, bool X) {
            const double XTIMES = 1.5;
            int DataNumber = 0;
            //お手本
            var ellipseRM = setEllipse(Brushes.Blue);
            var ellipseLM = setEllipse(Brushes.Blue);
            var lineRM = setLine(Brushes.Blue);
            var lineLM = setLine(Brushes.Blue);

            //右手
            if (i < MasterDatasetNumber) {
                if (X) {
                    Canvas.SetLeft(ellipseRM, 100 - Master[i].Z * XTIMES - 2.5);
                    lineRM.X2 = 100 - Master[i].Z * XTIMES;
                    Canvas.SetTop(ellipseRM, 100 + Master[i].X * XTIMES - 5);
                    lineRM.Y1 = 135;
                    lineRM.Y2 = 100 + Master[i].X * XTIMES;
                } else {
                    Canvas.SetLeft(ellipseRM, 100 - Master[i].Z - 2.5);
                    lineRM.X2 = 100 - Master[i].Z;
                    Canvas.SetTop(ellipseRM, 100 - Master[i].Y - 5);
                    lineRM.Y1 = 120;
                    lineRM.Y2 = 100 - Master[i].Y;
                }
                CanvasBody.Children.Add(ellipseRM);
                CanvasBody.Children.Add(lineRM);
            }
            //左手
            DataNumber = i + MasterDatasetNumber;
            if (DataNumber < (MasterDatasetNumber - 1) * 2) {
                if (X) {
                    Canvas.SetLeft(ellipseLM, 100 - Master[DataNumber].Z * XTIMES - 2.5);
                    lineLM.X2 = 100 - Master[DataNumber].Z * XTIMES;
                    Canvas.SetTop(ellipseLM, 100 + Master[DataNumber].X * XTIMES - 5.0);
                    lineLM.Y1 = 65;
                    lineLM.Y2 = 100 + Master[DataNumber].X * XTIMES;
                } else {
                    Canvas.SetLeft(ellipseLM, 100 - Master[DataNumber].Z - 2.5);
                    lineLM.X2 = 100 - Master[DataNumber].Z;
                    Canvas.SetTop(ellipseLM, 100 - Master[DataNumber].Y - 5.0);
                    lineLM.Y1 = 120;
                    lineLM.Y2 = 100 - Master[DataNumber].Y;
                }
                CanvasBody.Children.Add(ellipseLM);
                CanvasBody.Children.Add(lineLM);
            }


            //ビギナー
            DataNumber = 0;
            var ellipseRB = setEllipse(Brushes.Red);
            var ellipseLB = setEllipse(Brushes.Red);
            var lineRB = setLine(Brushes.Red);
            var lineLB = setLine(Brushes.Red);

            //右手
            if (i < BeginerDatasetNumber) {
                if (X) {
                    lineRB.X2 = 100 - Beginer[i].Z * 2;
                    Canvas.SetLeft(ellipseRB, 100 - Beginer[i].Z * 2 - 2.5);
                    Canvas.SetTop(ellipseRB, 100 + Beginer[i].X * 2 - 5.0);
                    lineRB.Y1 = 135;
                    lineRB.Y2 = 100 + Beginer[i].X * 2;
                } else {
                    lineRB.X2 = 100 - Beginer[i].Z;
                    Canvas.SetLeft(ellipseRB, 100 - Beginer[i].Z - 2.5);
                    Canvas.SetTop(ellipseRB, 100 - Beginer[i].Y - 5.0);
                    lineRB.Y1 = 120;
                    lineRB.Y2 = 100 - Beginer[i].Y;
                }
                CanvasBody.Children.Add(ellipseRB);
                CanvasBody.Children.Add(lineRB);
            }
            //左手
            DataNumber = i + BeginerDatasetNumber;
            if (DataNumber < (BeginerDatasetNumber - 1) * 2) {
                if (X) {
                    lineLB.X2 = 100 - Beginer[DataNumber].Z * 2;
                    Canvas.SetLeft(ellipseLB, 100 - Beginer[DataNumber].Z * 2 - 2.5);
                    Canvas.SetTop(ellipseLB, 100 + Beginer[DataNumber].X * 2 - 5.0);
                    lineLB.Y1 = 65;
                    lineLB.Y2 = 100 + Beginer[DataNumber].X * 2;
                } else {
                    lineLB.X2 = 100 - Beginer[DataNumber].Z;
                    Canvas.SetLeft(ellipseLB, 100 - Beginer[DataNumber].Z - 2.5);
                    Canvas.SetTop(ellipseLB, 100 - Beginer[DataNumber].Y - 5.0);
                    lineLB.Y1 = 120;
                    lineLB.Y2 = 100 - Beginer[DataNumber].Y;
                }
                CanvasBody.Children.Add(ellipseLB);
                CanvasBody.Children.Add(lineLB);
            }
        }

        private Ellipse setEllipse(Brush brush) {
            Ellipse ell = new Ellipse();
            ell.Width = 5;
            ell.Height = 10;
            ell.Stroke = brush;
            return ell;
        }

        private Line setLine(Brush brush) {
            Line line = new Line();
            line.Stroke = brush;
            line.StrokeThickness = 1;
            line.X1 = 100;
            return line;
        }
    }
}